#!/usr/bin/python3

import os
import json
import subprocess
import pymongo
from bson import ObjectId

HOST = os.getenv('MONGO_HOST', 'localhost')
PORT = int(os.getenv('MONGO_PORT', '27017'))

db_name = 'knowledge_test_bank'
test_banks_path = 'test-banks'

client = pymongo.MongoClient(HOST, PORT)
db = client[db_name]

class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)

def write_question_to_file(question:dict, collection:str):
    question_topic = question['topic'].replace(' ', '_')
    question_name = f'{question["question_name"]}.question.json'
    if 'group' in question:
        question_name = os.path.join(question['group']["group_name"], question_name)
    if len(db[collection].distinct('topic')) > 1:
        question_name = os.path.join(question_topic, question_name)

    final_path = os.path.join(test_banks_path, collection, question_name)
    os.makedirs(os.path.dirname(final_path), exist_ok=True)

    with open(os.path.abspath(final_path), 'w') as q_file:
        json.dump(question, q_file, indent=4, cls=JSONEncoder)


def main():
    test_bank_dirs = [f.path for f in os.scandir(test_banks_path) if f.is_dir()]
    for test_bank_dir in test_bank_dirs:
        test_bank_name = os.path.basename(test_bank_dir)
        for question in db[test_bank_name].find():
            write_question_to_file(question, test_bank_name)

if __name__ == "__main__":
    main()