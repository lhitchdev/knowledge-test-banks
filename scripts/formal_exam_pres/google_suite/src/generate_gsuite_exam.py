#! /usr/bin/python3

import os
import sys
import json
import argparse
import subprocess
# Move up the necessary amount of directories so the scripts path is inserted into sys.path
# In this case, it takes 4 directories to get to the scripts folder
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))))
import formal_exam_pres.google_suite.src.google_drive_handler as gdrive_handler

description = "This will generate a Google Form exam using the settings you specify when running this script and, " \
              "if creating an exam by topic breakout, in your topic breakout configuration file."

# The following params are used in argparse
allow_edits_default = False
allow_edits_desc = f"When true, users will be able to edit what was submitted. The default is '{allow_edits_default}'."
dont_collect_user_email_default = False
dont_collect_user_email_desc = "When specified, the value is true and user e-mail address will NOT be collected. " \
                               f"The default is '{dont_collect_user_email_default}'."
dont_make_form_a_quiz_default = False
dont_make_form_a_quiz_desc = "When specified, the value is true and the generated form is NOT set as a quiz. " \
                             f"The default is '{dont_make_form_a_quiz_default}."
dont_publish_summary_to_user_default = False
dont_publish_summary_to_user_desc = "When specified, the value is true and users are NOT given a link to view a " \
                                    "summary of responses after submitting their own response. " \
                                    f"The default is '{dont_publish_summary_to_user_default}'."
exam_description_default = ""
exam_description_desc = f"A description of the generated exam. The default is '{exam_description_default}'."
exam_is_organized_by_topic_default = False
exam_is_organized_by_topic_desc = "When specified, the value is true and results in question organization by " \
                                  "topic; otherwise questions appear in no particular order. " \
                                  f"The default is {exam_is_organized_by_topic_default}."
exam_title_default = "Knowledge Exam"
exam_title_desc = "The title for the exam to be generated. The default is a combination of the test bank json file " \
                  f"name, minus the extension, followed by '{exam_title_default}'. So with a file name of " \
                  f"'planner_MQF_20201231.json', the title would be 'Planner MQF 20201231 {exam_title_default}'. " \
                  "Specifying something other than the default results in the title as entered."
exam_topics_are_sorted_default = False
exam_topics_are_sorted_desc = "When specified, the value is true and results in sorted topics; otherwise organized " \
                              "topics appear in no particular order. When true, it implies the exam is to be " \
                              f"organized by topic. The default is {exam_topics_are_sorted_default}."
limit_to_one_submission_default = False
limit_to_one_submission_desc = "When specified, the value is true and users will only be able to make one " \
                               f"submission. The default is '{limit_to_one_submission_default}'."
testbank_json_file_desc = "This is a path to the file with the list of exams to be created. Each exam in the list " \
                          "contains its list of questions."
use_exam_at_index_default = 0
use_exam_at_index_desc = "This specifies which exam to use for test generation. The number represents the index of " \
                         "the exams list within the specified testbank_json_file. " \
                         f"The default is '{use_exam_at_index_default}'."
work_role_desc = f"The name of the work role being evaluated."


def get_exam_version(file_name: str) -> str:
    '''
    Purpose: This converts file names with underscores to spaces and changes the first character of a word that is NOT
    in all caps to a capital letter; the file extension is removed.
    :param file_name: The file name for processing
    :return: str - a string representing the exam version using the name of the generated file.
    '''
    exam_ver = os.path.splitext(os.path.basename(file_name.strip()))[0].split("_")
    for index, word in enumerate(exam_ver):
        if not word.isupper():
            # Only capitalize if this word is NOT in all caps
            exam_ver[index] = word.capitalize()
    return " ".join(exam_ver)


def get_config_params_json(args: argparse.Namespace) -> str:
    '''
    Purpose: This builds a dictionary of customizable Google Form features and settings.
    :param args: An argparse object with user requested values specified when executing at the command prompt.
    :return: A JSON string representing the requested user Google Form settings.
    '''
    exam_ver = get_exam_version(args.testbank_json_file)
    if args.exam_topics_are_sorted:
        # In order to imply the exam is to be organized by topic
        args.exam_is_organized_by_topic = True
    config_params = {
        "allowEdits": args.allow_edits,
        "collectUserEmail": not args.dont_collect_user_email,  # Because user value is true if email isn't collected
        "examDescription": args.exam_description,
        "examIsOrganizedByTopic": args.exam_is_organized_by_topic,
        "examTitle": args.exam_title,
        "examTopicsAreSorted": args.exam_topics_are_sorted,
        "limitToOneSubmission": args.limit_to_one_submission,
        "makeFormAQuiz": not args.dont_make_form_a_quiz,  # Because user value is true if the form isn't a quiz
        "publishSummaryToUser": not args.dont_publish_summary_to_user,  # Because user value is true if NOT published
        "workRole": args.work_role
    }
    if args.exam_title == exam_title_default and args.testbank_json_file.strip() != "":
        # If using the default exam title then add the file name to the exam title
        config_params["examTitle"] = f"{exam_ver} {exam_title_default}"
    return config_params


def get_exam_list(exam_file_path: str) -> list:
    '''
    Purpose: Opens a saved JSON file containing a list of exams, each exam being a list of questions on each exam, and
    converts it into a list object. The list of exams is specific to a single work-role.
    :param exam_file_path: A string representing the absolute or relative path to the list of exams.
    :return: A list representing the work-role exams.
    '''
    exams_list = []
    if exam_file_path.strip() == "":
        return exams_list

    with open(exam_file_path, "r") as exam_file_fp:
        exams_list = json.load(exam_file_fp)
    return exams_list


def main(args: argparse.Namespace):
    '''
    Purpose: This builds the necessary input parameters for the ExamCreator script and then executes the script. When
    the ExamCreator script is finished, a new exam will exist in the user's Google Drive.
    :param args: An argparse object with user requested values specified when executing at the command prompt.
    :return: None
    '''
    # Setup needed clasp params
    exam_list = get_exam_list(args.testbank_json_file)[args.use_exam_at_index]
    config_params = get_config_params_json(args)
    input_params = f"[{json.dumps(exam_list)},{json.dumps(config_params)}]"

    # Setup needed Google Drive Handler params
    args.name_of_html_file = config_params["examTitle"].replace(" ", "_") + "_urls.html"
    args.upload_image = True
    args.get_download_urls = False

    # Push images to Google Drive
    gdrive_handler.main(args)

    # Push the lasest ExamCreator script to the Google Apps Script Project
    echo_yes = subprocess.Popen(("echo", "y"), stdout=subprocess.PIPE)
    output = subprocess.check_output(("clasp", "push"), stdin=echo_yes.stdout)
    print(output.decode())

    # Run shell command and send its output to console
    output = subprocess.check_output(("clasp", "run", "main", "-p", input_params))
    print(output.decode())

    # Setup params to invoke the html file download feature
    args.upload_image = False
    args.get_download_urls = True
    
    # Get links to newly created form
    gdrive_handler.main(args)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("testbank_json_file", type=str, help=testbank_json_file_desc)
    parser.add_argument("token_path", type=str, help=gdrive_handler.TOKEN_PATH_DESC)
    parser.add_argument("dst_snip_dir_id", type=str, help=gdrive_handler.DST_SNIP_DIR_ID_DESC)
    parser.add_argument("work_role", type=str, help=work_role_desc)
    parser.add_argument("--allow_edits", action="store_true", help=allow_edits_desc)
    parser.add_argument("--dont_collect_user_email", action="store_true", help=dont_collect_user_email_desc)
    parser.add_argument("--dont_make_form_a_quiz", action="store_true", help=dont_make_form_a_quiz_desc)
    parser.add_argument("--dont_publish_summary_to_user", action="store_true", help=dont_publish_summary_to_user_desc)
    parser.add_argument("--exam_description", type=str, default=exam_description_default, help=exam_description_desc)
    parser.add_argument("--exam_is_organized_by_topic", action="store_true", help=exam_is_organized_by_topic_desc)
    parser.add_argument("--exam_title", type=str, default=exam_title_default, help=exam_title_desc)
    parser.add_argument("--exam_topics_are_sorted", action="store_true", help=exam_topics_are_sorted_desc)
    parser.add_argument("--image_ext", type=str, default=gdrive_handler.IMAGE_EXT_DEFAULT,
                        help=gdrive_handler.IMAGE_EXT_DESC)
    parser.add_argument("--limit_to_one_submission", action="store_true", help=limit_to_one_submission_desc)
    parser.add_argument("--min", type=int, default=gdrive_handler.MIN_DEFAULT, help=gdrive_handler.MIN_DESC)
    parser.add_argument("--msec", type=int, default=gdrive_handler.MSEC_DEFAULT, help=gdrive_handler.MSEC_DESC)
    parser.add_argument("--out_dir", type=str, default=gdrive_handler.OUT_DIR_DEFAULT, 
                        help=gdrive_handler.OUT_DIR_DESC)
    parser.add_argument("--scopes", nargs='+', type=str, default=gdrive_handler.SCOPES_DEFAULT,
                        help=gdrive_handler.SCOPES_DESC)
    parser.add_argument("--sec", type=int, default=gdrive_handler.SEC_DEFAULT, help=gdrive_handler.SEC_DESC)
    parser.add_argument("--src_snip_dir", type=str, default=gdrive_handler.SRC_SNIP_DIR_DEFAULT,
                        help=gdrive_handler.SRC_SNIP_DIR_DESC)
    parser.add_argument("--use_exam_at_index", type=int, default=use_exam_at_index_default, help=use_exam_at_index_desc)
    params = parser.parse_args()
    main(params)
