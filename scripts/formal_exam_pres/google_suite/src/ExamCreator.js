// Globals used in the form
const nameQuestionText = "Please provide your full name.";
const rankQuestionText = "Please provide your rank.";
const iqtQuestionText = "Did you go through IQT";
const trainingQuestionText = "Prior to taking this exam, did you complete training for ####?";
const correctAnswerTitle = "Correct Answer";
const questNameTitle = "Question Name";
const questUidTitle = "Question UID";
var questCounter = 1 // Stores the next available question number in the exam

// ======== CREATES FEEDBACK FORM AND SENDS LINK TO USER AFTER THEY SUBMIT EXAM =========================
function onFormSubmit(e){
  
  // ==================== GET INFO FROM TRIGGER EVENT =========
  let user = {time: e.namedValues['Timestamp'][0], email: e.namedValues['Email Address'][0]};
  Logger.log("---- user == " + user.email + "-----");
  
  let fileName = getSourceFileFromTrigger(e.triggerUid) + "_feedback";
  Logger.log("---- fileName == " + fileName + "-----");
  // ==========================================================
  
  
  // GET EXISTING FOLDER, OR CREATE A NEW ONE TO HOLD FEEDBACK
  let feedbackFolder = DriveApp.getFoldersByName("KnowledgeFeedback");
  let folder;
  while(feedbackFolder.hasNext()){
    folder = feedbackFolder.next();
    break;
  }
  if (!folder) {
    folder = DriveApp.createFolder("KnowledgeFeedback");
  }
  Logger.log("---- folder == " + folder);
  
 
  // ======================= SET TEXT AND QUESTIONS FOR FEEDBACK FORM ======================
  let intro = 'As a recent examinee, the Stan/Eval Section would appreciate your feedback about your exam experience. ' +
  'The details you provide will be used to enhance the exam itself as well as our processes and procedures. ' +  
  'We look forward to your candid thoughts. ' +  
  'Please contact the Stan/Eval Office with any questions or concerns. ' +
  'Thank you for your time! ' +
  'The Stan/Eval Office90IOS.DOT.INBOX@us.af.milCOM:  (210) 977-2561  DSN 969-2561 ';
    
  //Write feedback questions here and just plug in the variable
  let q1Title = "Do you feel you were adequately prepared for this exam?"
  let q1FollowUp = "If not, describe what areas you were not trained in or where you were not mentored properly:"
  let q2Title = "Did you have an adequate amount of time to take this exam?"
  let q2FollowUp = "How much more time do you belive should be given?"
  let q3Title = "Was the exam environment sufficient?"
  let q3FollowUp = "If not, describe what areas you were not trained in or where you were not mentored properly:"
  let q4Title = "Were the exam instructions clear?"
  let q4FollowUp = "If not, describe what areas you were not trained in or where you were not mentored properly:"
  let q5Title = "If there were any other points you would like to share with the Stan/Eval Office, enter them below:"
  let qFinalTitle = "May we contact you about this feedback?"
  let qFinalFollowUp= "If so, please include your contact information in the fields below:"
  
  
  
  // variable to hold our feedaback form
  let feedbackForm;
  
  //If the form doesnt exist, create it and generate questions.
  if (checkForFile(fileName) === false){
    feedbackForm = FormApp
    .create(fileName)
    .setDescription(intro)
    .setAllowResponseEdits(false)
    .setLimitOneResponsePerUser(false)
    .setProgressBar(true)
    .setAcceptingResponses(true)
    .setConfirmationMessage("Thank you for the feedback!");
    
    //Ask exam type
    let examType = feedbackForm.addMultipleChoiceItem();
    examType.setTitle("Evaluation Type:")
    .setChoices([
      examType.createChoice('Basic Dev'),
      examType.createChoice('Senior-Linux'),
      examType.createChoice('Senior-Windows'),
      examType.createChoice('Basic PO')
    ])
    .showOtherOption(false);
    
    //Q1
    createFeedbackQ(feedbackForm, q1Title, q1FollowUp);
    //Q2
    createFeedbackQ(feedbackForm, q2Title, q2FollowUp);
    //Q3
    createFeedbackQ(feedbackForm, q3Title, q3FollowUp);
    //Q4  
    createFeedbackQ(feedbackForm, q4Title, q4FollowUp);
    //Q5
    let q5Explain = feedbackForm.addParagraphTextItem()
    .setTitle(q5Title)
    .setRequired(false);
    //QFinal
    createFeedbackQ(feedbackForm, qFinalTitle, qFinalFollowUp);
    //Move feedback form to the "KnowledgeFeedback" folder
    moveFiles(feedbackForm,folder.getId());
    Logger.log("---- did not find fileName ----");

  } else {
    // if we already have a feedback have a form, grab it
    let files = DriveApp.getFilesByName(fileName);
    while(files.hasNext()){
      let file = files.next();
    }  
    if (file) {
      Logger.log("---- found fileName == " + fileName);
      feedbackForm = FormApp.openById(file.getId());
      Logger.log("---- found feedbackForm == " + feedbackForm);
    }
  }
   
  // ------- creates feedback response sheet -----------------------
  let responseSheet = fileName + "_responses";
  let feedbackResponseSheet;
  
  // If the reponse sheet doesnt exist, create it
  if(checkForFile(responseSheet) === false){
    feedbackResponseSheet = SpreadsheetApp.create(responseSheet);
    if (feedbackForm) {
      feedbackForm.setDestination(FormApp.DestinationType.SPREADSHEET, feedbackResponseSheet.getId());
      Logger.log("---- set form destination ----");
      
      //Move feedback form to the "KnowledgeFeedback" folder
      moveFiles(feedbackResponseSheet,folder.getId());
    }
  } 
  //If the response sheet exists, open 
  if(checkForFile(responseSheet) === true) {
    let files = DriveApp.getFilesByName(responseSheet)
    while(files.hasNext()){
      feedbackResponseSheet = files.next();
    } 
    feedbackResponseSheet = SpreadsheetApp.openById(feedbackResponseSheet.getId())
  }
  
  
  Logger.log("---- sent email to " + user.email + ". Link is " + feedbackForm.getPublishedUrl());
  sendEmail(user.email, user.time, feedbackForm.getPublishedUrl());   
}


// ======= FINDS SOURCE FILENAME WHERE TRIGGER OCCURRED =================
function getSourceFileFromTrigger(triggerUid) {
  let fileName = "";
  let allTriggers = ScriptApp.getProjectTriggers();
  let allTriggersLen = allTriggers.length;
  
  // iterate through triggers and match id's
  for (let i = 0; i < allTriggersLen; i++){
      let trigger = allTriggers[i];
      trigger.getTriggerSourceId();
      if (triggerUid == trigger.getUniqueId()){
        fileName = DriveApp.getFileById(trigger.getTriggerSourceId()).getName();
        break;
      }
  }
  return fileName;
}


// =========== CHECKS IF FILE EXISTS ================
function checkForFile(filename){
  return DriveApp.getFilesByName(filename).hasNext();
}


// =========== Generates an html file with links to edit, view results & view form ================
function saveFormLinks(form, html_file_name) {
  let preBody = "<!DOCTYPE html>\n<html>\n<head>\n  <title>" + form.getTitle() + " URLs</title>\n</head>\n  <body>\n";
  let postBody = "  </body>\n</html>";
  let body = "    <h2>To Edit Form:</h2><p><a href='" + form.getEditUrl() + "'>" + form.getTitle() + "</a></p>\n";
  body += "    <h2>To View Form Responses:</h2><p><a href='" + form.getSummaryUrl() + "'>" + form.getTitle() + "</a></p>\n";
  body += "    <h2>To Take The Exam:<h2><p><a href='" + form.getPublishedUrl() + "'>" + form.getTitle() + "</a></p>\n";
  let content = preBody + body + postBody;
  let html_files = DriveApp.getFilesByName(html_file_name);
  let html_file = undefined;
  if (html_files.hasNext()) {
    while (html_files.hasNext()) {
      html_file = html_files.next();
      html_file.setContent(content);
      break;
    }
  } else {
    html_file = DriveApp.createFile(html_file_name, content, MimeType.HTML);
    html_file.setSharing(DriveApp.Access.ANYONE, DriveApp.Permission.VIEW);
  }
  return html_file;
}


// =========== MOVES FILE TO TARGET DIRECTORY ==============
function moveFiles(item, targetFolderId) {
  let id = item.getId();
  let file = DriveApp.getFileById(id);
  file.getParents().next().removeFile(file);
  DriveApp.getFolderById(targetFolderId).addFile(file);
}


// ========== SENDS EMAIL TO ADDRESS ========================
function sendEmail(address, date, url){
  let response = UrlFetchApp.fetch(url);
  let htmlBody = HtmlService.createHtmlOutput(response).getContent();
  MailApp.sendEmail(address,
                    "Please provide us with feedback from your recent exam on " + date,
                    'This message requires HTML support to view.',
                    ("Please provide us with feedback from your recent exam by visiting this link " + url));
  Logger.log("Email sent to " + address);
}


function createFeedbackQ(feedbackForm, title, titleFollowUp){
  let q = feedbackForm.addMultipleChoiceItem();
  q.setTitle(title)
  .setChoices([
          q.createChoice('Yes'),
          q.createChoice('No'),
  ])
  .showOtherOption(false)
  .setRequired(true);
  let qExplain = feedbackForm.addParagraphTextItem()
  .setTitle(titleFollowUp)
  .setRequired(false);
}

    
// ============== POPULATE DROPDOWNS ON PAGE LOAD =================== 
//function onOpen() {
//  let sheet = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet();
////  let cellStart = 16; // hardcoded for now....
////  let folderName = "KnowledgeMQL";
////  
////  let fileList = getFileList(folderName);
////  let fileListLen = fileList.length;
////  if (fileListLen > 0) {
////    for (let i = 0; i < fileListLen; i++) {
////      let cell = sheet.getRange("A" + (cellStart + i));
////      cell.setValue(fileList[i]);
////    }
////  }
//}


// ============= GETS AN ARRAY OF FILES IN A FOLDER =================
function getFileList(folderName) {
  // get the question folder by name
  let folder = DriveApp.getFoldersByName(folderName).next();
  
  // grab filenames from folder
  let fileList = [];
  let files = folder.getFiles();
  while (files.hasNext()) {
    let currentFile = files.next()
    fileList.push(currentFile.getName());
  }
  return fileList;
}


// ============= GETS A SPECIFIC FILE IN A FOLDER =================
function getFile(folderName, mqfName) {
  // get the question folder by name
  let folder = DriveApp.getFoldersByName(folderName).next();
  
  // grab filenames from folder
  let files = folder.getFiles();
  let currentFile = null;
  while (files.hasNext()) {
    currentFile = files.next();
    if (currentFile.getName() === mqfName) {
      break;
    }
  }
  return currentFile;
}


// ============= GRABS AVAILABLE CHOICES FOR QUESTION ====================
function getChoices(options, answer, item) {
  let choices = [];
  Object.keys(options).forEach(function(key){
    // need to account for '0' and 'false' as potential answers
    if (options[key] !== '') {
      // create choice along with boolean for correct value; add to choices
      let choice = item.createChoice(options[key], key == answer.toUpperCase());
      choices.push(choice);
    }
  });
  return choices;
}


// ============ GETS IMG FROM FILENAME ===================================
function getImg(imageName) {
  let imgs = DriveApp.getFilesByName(imageName);
  let img;
  if (imgs.hasNext()) {
    img = imgs.next();
  }
  // return img object if found, or an empty object if not found
  return img;
}


// Gets the topic groupings in a dictionary like object
function getTopics(examList) {
  let topics = {};
  examList.forEach(function(question){
    if (!(question.topic in topics)){
      topics[question.topic] = [];
    }
    topics[question.topic].push(question);
  });
  return topics;
}


// Gets the topic groupings in a sorted dictionary like object
function getSortedTopics(examList) {
  let sortedTopics = {};
  let questionTopics = getTopics(examList);
  let topics = Object.keys(questionTopics);
  topics.sort();
  topics.forEach(function(topic) {
    if (!(topic in sortedTopics)) {
      sortedTopics[topic] = questionTopics[topic];
    }
  });
  return sortedTopics;
}


// Sorts input question list by topic names
function sortByTopic(examList, questionTopics) {
  let questions = [];
  if (questionTopics === undefined) {
    questionTopics = getTopics(examList);
  }
  Object.keys(questionTopics).forEach(function(topic){
    questions = questions.concat(questionTopics[topic]);
  });
  return questions;
}


// Sorts input question list by sorted topic names; the topics are in alphabetical order
function sortBySortedTopic(examList, sortedQuestionTopics) {
  let questions = [];
  if (sortedQuestionTopics === undefined) {
    sortedQuestionTopics = getSortedTopics(examList);
  }
  Object.keys(sortedQuestionTopics).forEach(function(topic){
    questions = questions.concat(sortedQuestionTopics[topic]);
  });
  return questions;
}


// Gets the question groups in a dictionary like object
function getQuestionGroups(examList) {
  let groups = {};
  examList.forEach(function(question){
    if ("group" in question){
      if (!(question.group.group_name in groups)) {
        groups[question.group.group_name] = [];
      }
      groups[question.group.group_name].push(question);
    }
  });
  return groups;
}


// Sorts input question list by question group name
function sortByGroup(examList, questionGroups) {
  let questions = [];
  if (questionGroups === undefined) {
    questionGroups = getQuestionGroups(examList);
  }
  let qGroups = JSON.parse(JSON.stringify(questionGroups)); // Leaves original object unaffected
  examList.forEach(function(question){
    if ("group" in question) {
      if (question.group.group_name in qGroups) {
        questions = questions.concat(qGroups[question.group.group_name]);
        delete qGroups[question.group.group_name];
      }
    } else {
      questions.push(question);
    }
  });
  return questions;
}


function addIDoNotKnowOption(options, lastOptionLetter) {
  // Purpose: Add an "I do not know" option at the end of all choices
  // param: options: A key => value object of the current multiple choice options
  // param: lastOptionLetter: An ASCII code representing the next available letter in the multiple choice options
  optionLetter = String.fromCharCode(lastOptionLetter);
  options[optionLetter] = optionLetter + ") " + "I do not know"
}


// Adds the question's image to the form; for question groups, the image is only added for the first question in the group
function addImageToForm(form, question, questionGroups) {
  let imgTitle = "Use This Image For Question " + questCounter;
  let skipImage = false;
  if ("group" in question) {
    // Handle images for group questions; this assumes the question list is already sorted so all questions in a group are together
    if (question.group.group_name in questionGroups) {
      if (questionGroups[question.group.group_name].length > 1) {
        // Change the Image title for question groups with more than one question
        imgTitle = "Use This Image For Questions " + questCounter + " to " + (questCounter + questionGroups[question.group.group_name].length - 1)
      }
      // Allow adding the image to the first question only; modifying the original object allows detection of questions with their images already included
      delete questionGroups[question.group.group_name];
    } else {
      // Prevent adding images for all other questions in the group
      skipImage = true;
    }
  }
  if (!skipImage) {
    let img = getImg(question.image_name);
    // make sure img returned isn't empty/NULL
    if (img) {
      form.addImageItem()
      .setImage(img.getThumbnail())
      .setTitle(imgTitle)
      .setAlignment(FormApp.Alignment.CENTER);
    }
  }
}


// ================= ADDS CHOICE QUESTIONS TO FORM ===============================
function addChoiceQuestionsToForm(form, questions, questionGroups, examIsOrganizedByTopic) {
  let prevSubject = "";
  // For easy access to choices/answer/explanation(s)
  let optionMap = {
    "A": 0,
    "B": 1,
    "C": 2,
    "D": 3,
    "E": 4,
    "F": 5,
    "G": 6,
    "H": 7,
    "I": 8
  };
  if (form === undefined) {
    // In case no parameters are specified
    console.log("ERROR: Missing Form");
    return;
  }
  if (questions === undefined) {
    // In case questions is NOT specified
    console.log("ERROR: Missing question list");
    return;
  }
  if (questionGroups === undefined) {
    // In case questionGroups is NOT specified
    questionGroups = getQuestionGroups(questions);
  }
  if (examIsOrganizedByTopic === undefined) {
    // In case examIsSortedByTopic is NOT specified
    examIsOrganizedByTopic = false;
  }
  questions.forEach(function(question){
    // set our variables for each question
    question.question = questCounter + ')  ' + question.question;
    let options = {};  // Stores all options for this multiple choice question
    let option = 'A'.charCodeAt(0);  // The current option
    question.choices.forEach(function(choice){
      let optionLetter = String.fromCharCode(option);
      options[optionLetter] = optionLetter + ") " + choice;
      ++option;
    });
    question.answer = Object.keys(optionMap)[question.answer];  // Convert index number to letter
    addIDoNotKnowOption(options, option)  // Add the I do not know option
    
    if (question.question_type === 'CHOICE') {
      // add a section header if it's the first question
      if (prevSubject !== question.topic && examIsOrganizedByTopic) {
        let header = form.addSectionHeaderItem();
        header.setTitle(question.topic);
      }
      // fetch and include image above question if one is specified
      if ("image_name" in question) {
        addImageToForm(form, question, questionGroups);
      }
      // add the question to the form
      let item = form.addMultipleChoiceItem();
      item.setTitle(question.question)
      .setChoices(getChoices(options, question.answer, item));
      // Setup the feedback given when the response is correct or incorrect
      let feedbackCorrect = FormApp.createFeedback();
      let feedbackIncorrect = FormApp.createFeedback();
      if (question.explanation.length > 1){
        // In case there is feedback for each option, setup the correct response feedback
        let opts = Object.keys(optionMap);  // A list of option letters starting with A
        for (let expIndex in question.explanation){
          // Insert each option letter in front of each explanation, storing it back in the array
          question.explanation[expIndex] = opts[expIndex] + ") " + question.explanation[expIndex].toString();
        }
        feedbackCorrect.setText(question.explanation[optionMap[question.answer]]);  // Setup correct feedback message
      } else {
        // In case there's only one explanation for all options, setup to display for correct responses
        feedbackCorrect.setText(question.explanation.toString());
      }
      item.setFeedbackForCorrect(feedbackCorrect.build());  // Add correct feedback to the item's correct feedback attribute
      feedbackIncorrect.setText(question.explanation.join("\n"));  // Setup incorrect feedback message
      item.setFeedbackForIncorrect(feedbackIncorrect.build());  // Add incorrect feedback to the item's incorrect feedback attribute
      item.setPoints(question.points);  // can cause errors if CHOICE doesn't have pointVal
      prevSubject = question.topic;
      questCounter++;
    }
  });
  // Temporarily disabled to see if this is still necessary
//  Utilities.sleep(50);  // Helps avoid form/resource access errors
}


// ======== ADDS GENERAL Q'S ASKED ON EVERY EXAM ===============
function addGeneralQuestionsToForm(form, workrole) {
  // name, rank and training/not
  form.addTextItem()
  .setTitle(nameQuestionText)
  .setRequired(true);
  
  form.addMultipleChoiceItem()
  .setTitle(trainingQuestionText.replace("####", workrole))
  .setChoiceValues(["Yes", "No"])
  .setRequired(true);
}

// ====== Adds a response to the form's questions. This allows us to see things like correct answers and question UIDs/names in the response sheet ======
function addResponseToForm(form, questionList, entryTitle, workrole) {
  let questionIndex = 0;  // To grab questions from questionList through iteration
  form.setCollectEmail(false);  // Necessary to add a response via script as there's no way to add an e-mail
  let formItems = form.getItems();  // Stores all questions on the form
  let formResponse = form.createResponse();
  
  formItems.forEach(function(item){
    // Set responses for each question on the form
    let itemsType = item.getType().toString();  // The item's type
    if (itemsType == "MULTIPLE_CHOICE"){
      let mcItem = item.asMultipleChoiceItem();
      let mcQuestion = mcItem.getTitle().replace(/^[0-9]+\)\s+/g, "");  // Strips off the leading #) before the question
      if (trainingQuestionText.replace("####", workrole) == mcQuestion){
        formResponse.withItemResponse(mcItem.createResponse("No"));  // Set training question response
      } else if (correctAnswerTitle == entryTitle){
        let mcChoices = mcItem.getChoices();
        mcChoices.forEach(function(choice){
          // Search choices for the correct answer
          if (choice.isCorrectAnswer()){
            formResponse.withItemResponse(mcItem.createResponse(choice.getValue()));  // Set correct answer response
            return;
          }
        });
      } else if (questUidTitle == entryTitle){
        mcItem.showOtherOption(true);  // Necessary to add information that's not a valid choice
        let qUid = questionList[questionIndex++]._id;
        formResponse.withItemResponse(mcItem.createResponse(qUid));  // Add the question's UID as a response
        mcItem.showOtherOption(false);  // Reset so examinee's don't have an other option
      } else if (questNameTitle == entryTitle){
        mcItem.showOtherOption(true);  // Necessary to add information that's not a valid choice
        let qName = questionList[questionIndex++].question_name;
        formResponse.withItemResponse(mcItem.createResponse(qName));  // Add the question's name as a response
        mcItem.showOtherOption(false);  // Reset so examinee's don't have an other option
      } else if (questUidTitle + ": " + questNameTitle == entryTitle){
        mcItem.showOtherOption(true);  // Necessary to add information that's not a valid choice
        let qUid = questionList[questionIndex]._id;
        let qName = questionList[questionIndex++].question_name;
        formResponse.withItemResponse(mcItem.createResponse(qUid + ": " + qName));  // Add the question's UID: name as a response
        mcItem.showOtherOption(false);  // Reset so examinee's don't have an other option
      }
    }
    if (itemsType == "TEXT"){
      let textItem = item.asTextItem();
      if (/full name/.test(textItem.getTitle().toLowerCase())){
        formResponse.withItemResponse(textItem.createResponse(entryTitle));  // Add the purpose of response as the full name
      }
    }
  });
  
  formResponse.submit();  // Send the responses to the form
  form.setCollectEmail(true);  // Reset so examinee's have to enter their e-mail address
  Utilities.sleep(50);  // Helps avoid form access errors
}

// ======================================================================================================
// This function creates a section break in the input form with the given title and description
// ======================================================================================================
function addSectionBreak(form, title, description){
  if (title === undefined){
    // In case the title wasn't provided as an input parameter
    title = "New Section"
  }
  if (description === undefined){
    // In case the description wasn't provided as an input parameter
    description = ""
  }
  form.addPageBreakItem().setTitle(title).setHelpText(description);
}

// ======================================================================================================
// This functions creates a form from the active sheet/tab in the currently active spreadsheet
// ======================================================================================================
function main(examQuestions, configParams) {
  if(examQuestions === undefined){
    console.log("ERROR: no exam questions were received");
    return
  }
  if(configParams === undefined){
    configParams = {};
    console.log("WARNING: no configuration parameters were received");
  }
  // Set default values if undefined; otherwise, set to the user requested value
  let examTitle = ("examTitle" in configParams) ? configParams.examTitle : "Knowledge Exam";
  let workRole = ("workRole" in configParams) ? configParams.workRole : "Basic Dev";
  let examDescription = ("examDescription" in configParams) ? configParams.examDescription : "";
  let allowEdits = ("allowEdits" in configParams) ? configParams.allowEdits : false;
  let limitToOneSubmission = ("limitToOneSubmission" in configParams) ? configParams.limitToOneSubmission : false;
  let collectUserEmail = ("collectUserEmail" in configParams) ? configParams.collectUserEmail : true;
  let publishSummaryToUser = ("publishSummaryToUser" in configParams) ? configParams.publishSummaryToUser : true;
  let makeFormAQuiz = ("makeFormAQuiz" in configParams) ? configParams.makeFormAQuiz : true;
  let testVersion = "Test Version: ";
  let examIsOrganizedByTopic = ("examIsOrganizedByTopic" in configParams) ? configParams.examIsOrganizedByTopic : false;
  let examTopicsAreSorted = ("examTopicsAreSorted" in configParams) ? configParams.examTopicsAreSorted : false;
  
  let form = FormApp
  .create(examTitle)  // grab and set the title of the form
  .setTitle(examTitle)  // Ensure .getTitle() has a value
  .setDescription(examDescription + "\n" + testVersion + examTitle) // Set according to User Desired Value for `Description/Instructions`
  .setAllowResponseEdits(allowEdits) // cannot edit answers after submitting
  .setLimitOneResponsePerUser(limitToOneSubmission)  // only allow a single reponse from each user
  .setCollectEmail(collectUserEmail)  // collect email from each submission
  .setPublishingSummary(publishSummaryToUser)
  .setProgressBar(true)
  .setShowLinkToRespondAgain(false)
  .setIsQuiz(makeFormAQuiz);
      // time limit?
      // any other important data
      
  //=============== CREATE THE RESPONSE SPREASHEET ====================
  let responseSheet = SpreadsheetApp.create(examTitle + '_responses');
  form.setDestination(FormApp.DestinationType.SPREADSHEET, responseSheet.getId());
  
  // ============== ADD GENERAL QUESTIONS TO FORM ======================
  addGeneralQuestionsToForm(form, workRole);
  addSectionBreak(form, "Exam");
   
  // =============== ADD QUESTIONS TO FORM BASED ON USER INPUT =========
  // Sort exam by any existing group questions
  let questionGroups = getQuestionGroups(examQuestions);
  examQuestions = sortByGroup(examQuestions, questionGroups);
  
  // Sort exam according to desired topic organization option
  if (examIsOrganizedByTopic) {
    if (examTopicsAreSorted) {
      examQuestions = sortBySortedTopic(examQuestions);
    } else {
      examQuestions = sortByTopic(examQuestions);
    }
  }
  
  addChoiceQuestionsToForm(form, examQuestions, questionGroups, examIsOrganizedByTopic);
  addSectionBreak(form, "Review & Submit", "Please review your answers (by clicking the back button below) " +
                  "to make sure you've answered every question to your satisfaction. Once you're happy with your answers, " +
                  "select submit to complete your exam and receive your grade.");
  
  // Add the correct answers as a response for access in the Google Sheet copy of responses
  addResponseToForm(form, examQuestions, correctAnswerTitle, workRole);
  // Add question identifying information as a response in order to update the Test Bank with the results
  addResponseToForm(form, examQuestions, questUidTitle + ": " + questNameTitle, workRole);
  // Create HTML file with links to edit, view responses, and view form
  let html_file_name = examTitle.replace(/\s+/g, "_") + "_urls.html";
  saveFormLinks(form, html_file_name);
}
