import unittest
import sys
import os
import argparse
import json
import shutil
import copy
# Move up the necessary amount of directories so the scripts path is inserted into sys.path
# In this case, it takes 4 directories to get to the scripts folder
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))))))
from scripts.formal_exam_pres.google_suite.src import generate_gsuite_exam


def create_files(path: str, data):
    if not os.path.exists(os.path.dirname(path)):
        os.mkdir(os.path.dirname(path))
    with open(path, "w") as path_fp:
        path_fp.write(data)


def create_json_files(path: str, data):
    if not os.path.exists(os.path.dirname(path)):
        os.mkdir(os.path.dirname(path))
    with open(path, "w") as path_fp:
        json.dump(data, path_fp)


def remove_temp_files(temp_folder: str):
    if os.path.exists(temp_folder):
        shutil.rmtree(temp_folder)


class InputArgs(argparse.Namespace):
    def __init__(self, testbank_json_file: str, token_path: str, dst_snip_dir_id: str, allow_edits: bool = False, 
                 dont_collect_user_email: bool = False, dont_make_form_a_quiz: bool = False, 
                 dont_publish_summary_to_user: bool = False, exam_description: str = "", 
                 exam_is_organized_by_topic: bool = False, exam_title: str = "Knowledge Exam", 
                 exam_topics_are_sorted: bool = False, image_ext: str = ".png", 
                 limit_to_one_submission: bool = False, min: int = 1, msec: int = 0, out_dir: str = "FormLinks", 
                 scopes: list = None, sec: int = 30, src_snip_dir: str = "code_snippets", use_exam_at_index: int = 0, 
                 work_role: str = "Basic Dev"):
        self.testbank_json_file = testbank_json_file
        self.token_path = token_path
        self.dst_snip_dir_id = dst_snip_dir_id
        self.allow_edits = allow_edits
        self.dont_collect_user_email = dont_collect_user_email
        self.dont_make_form_a_quiz = dont_make_form_a_quiz
        self.dont_publish_summary_to_user = dont_publish_summary_to_user
        self.exam_description = exam_description
        self.exam_is_organized_by_topic = exam_is_organized_by_topic
        self.exam_title = exam_title
        self.exam_topics_are_sorted = exam_topics_are_sorted
        self.image_ext = image_ext
        self.limit_to_one_submission = limit_to_one_submission
        self.min = min
        self.msec = msec
        self.out_dir = out_dir
        self.scopes = scopes if scopes is not None else [
            "https://www.googleapis.com/auth/drive",
            "https://www.googleapis.com/auth/drive.file"
        ]
        self.sec = sec
        self.src_snip_dir = src_snip_dir
        self.use_exam_at_index = use_exam_at_index
        self.work_role = work_role


class GetExamVersionTests(unittest.TestCase):
    testing_dir = "Testing_get_exam_version_Files"
    path_all_caps = "THIS_PATH_IS_ALL_CAPS.json"
    path_one_word_all_caps = "only_one_IS_all_caps.json"
    path_mixed_caps = "This_hAs_MIXED_caps.json"
    path_has_dash_and_underscore = "This_has_dash-underscore.json"

    def test_get_exam_version_all_caps(self):
        self.expected_result = "THIS PATH IS ALL CAPS"
        self.path = os.path.join(self.testing_dir, self.path_all_caps)
        self.assertEqual(self.expected_result, generate_gsuite_exam.get_exam_version(self.path))

    def test_get_exam_version_one_word_all_caps(self):
        self.expected_result = "Only One IS All Caps"
        self.path = os.path.join(self.testing_dir, self.path_one_word_all_caps)
        self.assertEqual(self.expected_result, generate_gsuite_exam.get_exam_version(self.path))

    def test_get_exam_version_mixed_caps(self):
        self.expected_result = "This Has MIXED Caps"
        self.path = os.path.join(self.testing_dir, self.path_mixed_caps)
        self.assertEqual(self.expected_result, generate_gsuite_exam.get_exam_version(self.path))

    def test_get_exam_version_has_dash_and_underscore(self):
        self.expected_result = "This Has Dash-underscore"
        self.path = os.path.join(self.testing_dir, self.path_has_dash_and_underscore)
        self.assertEqual(self.expected_result, generate_gsuite_exam.get_exam_version(self.path))

    def test_get_exam_version_whitespace(self):
        self.expected_result = ""
        self.assertEqual(self.expected_result, generate_gsuite_exam.get_exam_version("   "))


class GetConfigParamsTests(unittest.TestCase):
    path_mixed_caps = "This_hAs_MIXED_caps.json"
    exam_title_mixed_caps = "This Has MIXED Caps"

    def test_get_config_params_defaults(self):
        self.args = InputArgs("", "", "")
        self.expected_result = {
            "allowEdits": self.args.allow_edits,
            "collectUserEmail": not self.args.dont_collect_user_email,
            "examDescription": self.args.exam_description,
            "examIsOrganizedByTopic": self.args.exam_is_organized_by_topic,
            "examTitle": self.args.exam_title,
            "examTopicsAreSorted": self.args.exam_topics_are_sorted,
            "limitToOneSubmission": self.args.limit_to_one_submission,
            "makeFormAQuiz": not self.args.dont_make_form_a_quiz,
            "publishSummaryToUser": not self.args.dont_publish_summary_to_user,
            "workRole": self.args.work_role
        }
        self.expected_result = self.expected_result
        self.assertEqual(self.expected_result, generate_gsuite_exam.get_config_params_json(self.args))

    def test_get_config_params_opposite_bools(self):
        self.args = InputArgs(self.path_mixed_caps, "token_file", "dst_id", allow_edits=True, 
                              dont_collect_user_email=True, dont_make_form_a_quiz=True, 
                              dont_publish_summary_to_user=True, limit_to_one_submission=True, 
                              exam_is_organized_by_topic=True, exam_topics_are_sorted=True)
        self.expected_result = {
            "allowEdits": self.args.allow_edits,
            "collectUserEmail": not self.args.dont_collect_user_email,
            "examDescription": self.args.exam_description,
            "examIsOrganizedByTopic": self.args.exam_is_organized_by_topic,
            "examTitle": f"{self.exam_title_mixed_caps} {self.args.exam_title}",
            "examTopicsAreSorted": self.args.exam_topics_are_sorted,
            "limitToOneSubmission": self.args.limit_to_one_submission,
            "makeFormAQuiz": not self.args.dont_make_form_a_quiz,
            "publishSummaryToUser": not self.args.dont_publish_summary_to_user,
            "workRole": self.args.work_role
        }
        self.expected_result = self.expected_result
        result = generate_gsuite_exam.get_config_params_json(self.args)
        self.assertEqual(self.expected_result, result)


class GetExamListTests(unittest.TestCase):
    testing_dir = "Testing_get_topic_breakout_Files"
    exams_file_one = "PO_one_MQF_20201228.json"
    exams_file_two = "PO_tWo_MQF_20201228.json"
    exams_file_five = "PO_fIVe_MQF_20201228.json"
    gend_exams_one = '[[{"_id": "5fa0d2848bc9ca2dd42480cf", "old_id": "BPO_0082", "rel-link_id": ' \
                     '"5f52396f6405c97bbb5964ac", "question_name": "po-agile-outcome-output", "question_path": ' \
                     '"PO/po-agile-outcome-output", "question_type": "CHOICE", "topic": "Agile", "disabled": false, ' \
                     '"provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "What is the ' \
                     'relationship between outcome and output?", "choices": ["Outcome focuses on functionality, ' \
                     'output focus is on value", "Outcome focuses on quantity, output focus is on quality", ' \
                     '"Outcome focuses on quality, output focus is on quantity", "Outcome focuses on velocity, ' \
                     'output focus is on value"], "answer": 2, "explanation": ["Outcome focuses on quality, output ' \
                     'focus is on quantity."], "estimated_time_to_complete": 0.9, "points": 1, "weight": 1.0, ' \
                     '"q_weight_scalar": 1.0}, ' \
                     '{"_id": "5fa0d2848bc9ca2dd424812f", "old_id": "BPO_0075", "rel-link_id": ' \
                     '"5f52396f6405c97bbb5964aa", "question_name": "po-agile-decision-making", "question_path": ' \
                     '"PO/po-agile-decision-making", "question_type": "CHOICE", "topic": "Agile", "disabled": false, ' \
                     '"provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "Which of the ' \
                     'following is a decision-making approach a Product Owner might use?", "choices": ' \
                     '["Authoritative", "Majority Vote", "Deferring", "Unanimous Vote"], "answer": 1, "explanation": ' \
                     '["Common approaches are: majority vote, decide after discussion, and delegate."], ' \
                     '"estimated_time_to_complete": 0.9, "points": 1, "weight": 1.0, "q_weight_scalar": 1.0}, ' \
                     '{"_id": "5fa0d2838bc9ca2dd424802d", "old_id": "BPO_0099", "rel-link_id": ' \
                     '"5f52396f6405c97bbb5964a1", "question_name": "po-agile-scrum-roles", "question_path": ' \
                     '"PO/po-agile-scrum-roles", "question_type": "CHOICE", "topic": "Agile", "disabled": false, ' \
                     '"provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "Which of the ' \
                     'following identify Scrum roles?", "choices": ["Product Owner, Development Team, and Tester", ' \
                     '"Project Manager, Product Owner, Development Team, and Scrum Master", "Product Owner, ' \
                     'Development Team, Tester, and Scrum Master", "Product Owner, Development Team, and Scrum ' \
                     'Master"], "answer": 3, "explanation": ["A scrum team consists of a Product Owner, Development ' \
                     'Team, and Scrum Master."], "estimated_time_to_complete": 0.9, "points": 1, "weight": 1.0, ' \
                     '"q_weight_scalar": 1.0}, ' \
                     '{"_id": "5fa0d2838bc9ca2dd4247fe5", "old_id": "BPO_0081", "rel-link_id": ' \
                     '"5f52396f6405c97bbb5964b0", "question_name": "po-agile-front-door", "question_path": ' \
                     '"PO/po-agile-front-door", "question_type": "CHOICE", "topic": "Agile", "disabled": false, ' \
                     '"provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "The Front Door ' \
                     'process takes a Potential Requirement and validates it to a _________________________.", ' \
                     '"choices": ["Concrete Requirement", "Known Good Requirement", "Value-Based Requirement", ' \
                     '"Functional Requirement"], "answer": 1, "explanation": ["From our Squadron OI: Once a ' \
                     'Potential Requirement has been validated, it is becomes a validated Known Good Requirement. ' \
                     'Known Good Requirements are requirements that are ready to be matured by an appointed PO."], ' \
                     '"estimated_time_to_complete": 0.9, "points": 1, "weight": 1.0, "q_weight_scalar": 1.0}, ' \
                     '{"_id": "5fa0d2848bc9ca2dd4248057", "old_id": "BPO_0094", "rel-link_id": ' \
                     '"5f52396f6405c97bbb5964af", "question_name": "po-agile-values", "question_path": ' \
                     '"PO/po-agile-values", "question_type": "CHOICE", "topic": "Agile", "disabled": false, ' \
                     '"provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "Which of the ' \
                     'following is NOT an Agile value?", "choices": ["individuals and interations over processes and ' \
                     'tools", "working software over comprehensive documentation", "leadership collaboration over ' \
                     'customer collaboration", "responding to change over following a plan"], "answer": 2, ' \
                     '"explanation": ["The Value is: customer collaboration over contract negotiations."], ' \
                     '"estimated_time_to_complete": 0.9, "points": 1, "weight": 1.0, "q_weight_scalar": 1.0}]]'
    gend_exams_two = '[[{"_id": "5fa0d2838bc9ca2dd4247f79", "old_id": "BPO_0078", "rel-link_id": ' \
                     '"5f52396f6405c97bbb5964a7", "question_name": "po-agile-estimates", "question_path": ' \
                     '"PO/po-agile-estimates", "question_type": "CHOICE", "topic": "Scrum", "disabled": false, ' \
                     '"provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "The Product Owner ' \
                     'wants to create estimations and asks the Scrum Master for guidance about it.  What is the ' \
                     'proper guidance that the Scrum Master should give?", "choices": ["There are no estimates in ' \
                     'Scrum.", "Development Team determines the estimations.", "Product Owner should use ideal ' \
                     'number of days to get estimations.", "Product Owner should use story points to get ' \
                     'estimations."], "answer": 1, "explanation": ["Product Owners do not estimate tasks."], ' \
                     '"estimated_time_to_complete": 0.9, "points": 1, "weight": 1.0, "q_weight_scalar": 1.0}, ' \
                     '{"_id": "5fa0d2838bc9ca2dd4247f8b", "old_id": "BPO_0089", "rel-link_id": ' \
                     '"5f52396f6405c97bbb5964a8", "question_name": "po-scrum-pillars", "question_path": ' \
                     '"PO/po-scrum-pillars", "question_type": "CHOICE", "topic": "Scrum", "disabled": false, ' \
                     '"provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "Which of the ' \
                     'following is NOT a Scrum pillar?", "choices": ["Iteration", "Transparancy", "Adaption", ' \
                     '"Inspection"], "answer": 0, "explanation": ["Transparancy, Inspection, and Adaption make up ' \
                     'the three Scrum Pillars."], "estimated_time_to_complete": 0.9, "points": 1, "weight": 1.0, ' \
                     '"q_weight_scalar": 1.0}, ' \
                     '{"_id": "5fa0d2858bc9ca2dd424818f", "old_id": "BPO_0098", "rel-link_id": ' \
                     '"5f52396f6405c97bbb59649c", "question_name": "po-agile-sprint-purpose", "question_path": ' \
                     '"PO/po-agile-sprint-purpose", "question_type": "CHOICE", "topic": "Scrum", "disabled": false, ' \
                     '"provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "What is the main ' \
                     'purpose of the Sprint?", "choices": ["To complete as many User Stories as possible", "To ' \
                     'complete development and testing of all new planned features", "To provide work that answers ' \
                     'to stakeholder questions", "To produce a releaseable or shippable working product increment"], ' \
                     '"answer": 3, "explanation": ["The goal of each sprint should be to produce a releasable ' \
                     'product."], "estimated_time_to_complete": 0.9, "points": 1, "weight": 1.0, "q_weight_scalar": ' \
                     '1.0}, ' \
                     '{"_id": "5fa0d2848bc9ca2dd424807b", "old_id": "BPO_0093", "rel-link_id": ' \
                     '"5f52396f6405c97bbb5964a3", "question_name": "po-agile-cannot-complete", "question_path": ' \
                     '"PO/po-agile-cannot-complete", "question_type": "CHOICE", "topic": "Scrum", "disabled": false, ' \
                     '"provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "During the Sprint ' \
                     'the Development Team determines that it cannot complete its work by the end of the Sprint. ' \
                     'What happens in this case?", "choices": ["The Sprint will be cancelled.", "The Development ' \
                     'Team will remove items they cannot complete from the Sprint.", "The Sprint will continue and ' \
                     'unfinished work will return to the Backlog.", "The Sprint will be extended."], "answer": 2, ' \
                     '"explanation": ["Any work not completed is moved to the backlog and the Sprint continues as ' \
                     'planned."], "estimated_time_to_complete": 0.9, "points": 1, "weight": 1.0, "q_weight_scalar": ' \
                     '1.0}, ' \
                     '{"_id": "5fa0d2838bc9ca2dd424800f", "old_id": "BPO_0083", "rel-link_id": ' \
                     '"5f52396f6405c97bbb5964a6", "question_name": "po-agile-estimate-2", "question_path": ' \
                     '"PO/po-agile-estimate-2", "question_type": "CHOICE", "topic": "Scrum", "disabled": false, ' \
                     '"provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "The Product Owner of ' \
                     'a team was asked to provide a forecast for the completion of the product release.  There are ' \
                     '140 story points worth of work remaining for the project.  The team\'s average velocity in the ' \
                     'past three (3) Sprints has been 65, though the best Sprint among the last three had a velocity ' \
                     'of 85 story points.  This is the proper Product Owner\'s response.", "choices": ["What is the ' \
                     'team will stretch themselves in the last two (2) Sprints to complete the release?", "What is ' \
                     'the team will need about three (3) more Sprints to complete the release?", "What is the team ' \
                     'will likely finish development in two (2) Sprints, but all of the testing may not finish in ' \
                     'time?", "What is the team will need to complete one more Sprint before an assessment can be ' \
                     'made?"], "answer": 1, "explanation": ["Because the points are more than usually accomplished ' \
                     'for two sprints based on the average velocity, the team should plan for a third sprint."], ' \
                     '"estimated_time_to_complete": 0.9, "points": 1, "weight": 1.0, "q_weight_scalar": 1.0}], ' \
                     '[{"_id": "5fa0d2848bc9ca2dd424810b", "old_id": "BPO_0085", "rel-link_id": ' \
                     '"5f52396f6405c97bbb59649d", "question_name": "po-agile-commander", "question_path": ' \
                     '"PO/po-agile-commander", "question_type": "CHOICE", "topic": "Scrum", "disabled": false, ' \
                     '"provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "The Squadron ' \
                     'Commander asks the Development Team to add a very important story to the current Sprint. What ' \
                     'should the Team do?", "choices": ["Respect the Commander\'s authority and add the story to ' \
                     'the current Sprint without any adjustments.", "Add the story to the next Sprint.", "Inform the ' \
                     'Product Owner so he/she can work with the Commander.", "Add the story to the current Sprint ' \
                     'and drop a story of equal size."], "answer": 2, "explanation": ["The product owner needs to ' \
                     'work with all stakeholders to define backlog items. No new items should be brought into the ' \
                     'current Sprint."], "estimated_time_to_complete": 0.9, "points": 1, "weight": 1.0, ' \
                     '"q_weight_scalar": 1.0}, ' \
                     '{"_id": "5fa0d2838bc9ca2dd4247f85", "old_id": "BPO_0091", "rel-link_id": ' \
                     '"5f52396f6405c97bbb5964a4", "question_name": "po-agile-ceremonies", "question_path": ' \
                     '"PO/po-agile-ceremonies", "question_type": "CHOICE", "topic": "Scrum", "disabled": false, ' \
                     '"provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "Which of the ' \
                     'following is NOT an Agile Scrum Ceremony?", "choices": ["Daily Scrum", "Sprint Planning", ' \
                     '"Product Demonstration", "Sprint Review"], "answer": 2, "explanation": ["Demonstration may be ' \
                     'a part of the Sprint Review ceremony but it is not a ceremony in itself."], ' \
                     '"estimated_time_to_complete": 0.9, "points": 1, "weight": 1.0, "q_weight_scalar": 1.0}, ' \
                     '{"_id": "5fa0d2838bc9ca2dd4248027", "old_id": "BPO_0079", "rel-link_id": ' \
                     '"5f52396f6405c97bbb59649b", "question_name": "po-agile-sprint-ceremony-review", ' \
                     '"question_path": "PO/po-agile-sprint-ceremony-review", "question_type": "CHOICE", "topic": ' \
                     '"Scrum", "disabled": false, "provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, ' \
                     '"question": "Which Scrum ceremony does the team share progress with stakeholders?", ' \
                     '"choices": ["Sprint Retrospective", "Sprint Planning", "Daily Scrum", "Sprint Review"], ' \
                     '"answer": 3, "explanation": ["The Sprint Review is where the team presents progress to the ' \
                     'stakeholders."], "estimated_time_to_complete": 0.9, "points": 1, "weight": 1.0, ' \
                     '"q_weight_scalar": 1.0}, ' \
                     '{"_id": "5fa0d2838bc9ca2dd4247f6d", "old_id": "BPO_0054", "rel-link_id": ' \
                     '"5ee9250a761cf87371cf3114", "question_name": "po-scrum_5", "question_path": "PO/po-scrum_5", ' \
                     '"question_type": "CHOICE", "topic": "Product Owner", "disabled": false, "provisioned": 0, ' \
                     '"attempts": 0, "passes": 0, "failures": 0, "question": "Which role is the equivalent of ' \
                     'Project Manager in Scrum?", "choices": ["The Product Owner", "The Team Leader(s)", "The ' \
                     'Development Team", "The Scrum Master", "None of the above"], "answer": 4, "explanation": ["The ' \
                     'project management activities are distributed among all three roles."], ' \
                     '"estimated_time_to_complete": 0.9, "points": 1, "weight": 1.0, "q_weight_scalar": 1.0}, ' \
                     '{"_id": "5fa0d2848bc9ca2dd4248051", "old_id": "BPO_0007", "rel-link_id": ' \
                     '"5ee9250b761cf87371cf3130", "question_name": "po-functional-reqs", "question_path": ' \
                     '"PO/po-functional-reqs", "question_type": "CHOICE", "topic": "Product Owner", "disabled": ' \
                     'false, "provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "The ' \
                     'Development Team cannot deliver the Increment because they don\'t understand a functional ' \
                     'requirement. What should they do?", "choices": ["Partially complete the work", "Move the work ' \
                     'to a future Sprint", "Ask a specialist to solve the problem within the remaining duration of ' \
                     'the Sprint", "Collaborate with the Product Owner to solve the problem"], "answer": 3, ' \
                     '"explanation": ["The Product Owner is responsible to make all items clear and understood. If ' \
                     'the problem wasn\'t solved, then there\'s no option other than leaving it to go back to the ' \
                     'Product Backlog at the end of the Sprint and be done later. The team is cross-functional and ' \
                     'doesn\'t use external help. We don\'t deliver partially complete items; everything should be ' \
                     'done based on the definition of \'Done\'."], "estimated_time_to_complete": 0.9, "points": 1, ' \
                     '"weight": 1.0, "q_weight_scalar": 1.0}]]'
    gend_exams_five = '[[{"_id": "5fa0d2858bc9ca2dd4248177", "old_id": "BPO_0073", "rel-link_id": ' \
                      '"5ee9250b761cf87371cf3155", "question_name": "po-velocity_2", "question_path": ' \
                      '"PO/po-velocity_2", "question_type": "CHOICE", "topic": "Product Owner", "disabled": false, ' \
                      '"provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "A key measure of ' \
                      'success for the Product Owner is velocity.", "choices": ["True", "False"], "answer": 1, ' \
                      '"explanation": ["Velocity is a relative concept which cannot be compared with other projects ' \
                      'and other teams. Even when it\'s considered inside the project, while it\'s important, it\'s ' \
                      'not the key measure of success. The Product Owner should be focused on how much value is ' \
                      'delivered and anything else might be misleading."], "estimated_time_to_complete": 0.9, ' \
                      '"points": 1, "weight": 1.0, "q_weight_scalar": 1.0}, ' \
                      '{"_id": "5fa0d2838bc9ca2dd4247ff1", "old_id": "BPO_0018", "rel-link_id": ' \
                      '"5ee9250a761cf87371cf3124", "question_name": "po-product-backlog_5", "question_path": ' \
                      '"PO/po-product-backlog_5", "question_type": "CHOICE", "topic": "Product Owner", "disabled": ' \
                      'false, "provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "Recently, ' \
                      'some changes occurred in the environment where the product will be used. How does this impact ' \
                      'the Product Backlog?", "choices": ["The Product Backlog remains the same.", "The Product ' \
                      'Backlog will be updated with those changes", "The Product Backlog should be kept high-level ' \
                      'enough to tolerate such changes", "A new Product Backlog baseline is created after saving the ' \
                      'old one"], "answer": 1, "explanation": ["The Product Backlog is a dynamic concept that is ' \
                      'always changing to reflect feedback from various entities."], "estimated_time_to_complete": ' \
                      '0.9, "points": 1, "weight": 1.0, "q_weight_scalar": 1.0}, ' \
                      '{"_id": "5fa0d2838bc9ca2dd4247ff7", "old_id": "BPO_0070", "rel-link_id": ' \
                      '"5ee9250b761cf87371cf3125", "question_name": "po-time_1", "question_path": "PO/po-time_1", ' \
                      '"question_type": "CHOICE", "topic": "Product Owner", "disabled": false, "provisioned": 0, ' \
                      '"attempts": 0, "passes": 0, "failures": 0, "question": "What percentage of time should the ' \
                      'Product Owner dedicate to the team", "choices": ["100%", "50%", "As much time as is needed to ' \
                      'maximize value", "Enough to refine the backlog"], "answer": 2, "explanation": ["Since ' \
                      'environments can be different the Product Owner should spend as much time as is needed to ' \
                      'maximize value."], "estimated_time_to_complete": 0.9, "points": 1, "weight": 1.0, ' \
                      '"q_weight_scalar": 1.0}, ' \
                      '{"_id": "5fa0d2848bc9ca2dd4248111", "old_id": "BPO_0051", "rel-link_id": ' \
                      '"5ee9250b761cf87371cf3145", "question_name": "po-scrum_1", "question_path": "PO/po-scrum_1", ' \
                      '"question_type": "CHOICE", "topic": "Product Owner", "disabled": false, "provisioned": 0, ' \
                      '"attempts": 0, "passes": 0, "failures": 0, "question": "Scrum is an iterative and incremental ' \
                      'Agile framework.", "choices": ["True", "False"], "answer": 0, "explanation": ["Scrum is ' \
                      'iterative because processes are repeated in each Sprint. It\'s incremental because it ' \
                      'delivers the product in multiple incremental steps instead of in a big bang. And of course ' \
                      'it\'s Agile (adaptive instead of predictive)."], "estimated_time_to_complete": 0.9, "points": ' \
                      '1, "weight": 1.0, "q_weight_scalar": 1.0}, ' \
                      '{"_id": "5fa0d2838bc9ca2dd4247f73", "old_id": "BPO_0049", "rel-link_id": ' \
                      '"5ee9250a761cf87371cf3115", "question_name": "po-scrum-master_1", "question_path": ' \
                      '"PO/po-scrum-master_1", "question_type": "CHOICE", "topic": "Product Owner", "disabled": ' \
                      'false, "provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "The Scrum ' \
                      'Master is a management role.", "choices": ["True", "False"], "answer": 0, "explanation": ' \
                      '["The Scrum Master is a management role but for the process rather than for the people."], ' \
                      '"estimated_time_to_complete": 0.9, "points": 1, "weight": 1.0, "q_weight_scalar": 1.0}], ' \
                      '[{"_id": "5fa0d2848bc9ca2dd4248123", "old_id": "BPO_0068", "rel-link_id": ' \
                      '"5ee9250b761cf87371cf3148", "question_name": "po-time-management_1", "question_path": ' \
                      '"PO/po-time-management_1", "question_type": "CHOICE", "topic": "Product Owner", "disabled": ' \
                      'false, "provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "When a ' \
                      'Product Owner works with the team, how much time should be devoted?","choices": ["The amount  ' \
                      'dictated by senior leadership", "All of their time", "The amount needed to complete their ' \
                      'responsibilities", "The amount needed by the customer"], "answer": 2, "explanation": ["The ' \
                      'Product Owner should spend the amount of time needed to complete all their responsibilities ' \
                      'that maximizes value. Note, the Product Owner should spend time with developers for things ' \
                      'like refactoring, sprint planning, answering questions during the Sprint, and helping ' \
                      'developers adjust their work."], "estimated_time_to_complete": 0.9, "points": 1, "weight": ' \
                      '1.0, "q_weight_scalar": 1.0}, ' \
                      '{"_id": "5fa0d2848bc9ca2dd42480ed", "old_id": "BPO_0052", "rel-link_id": ' \
                      '"5ee9250b761cf87371cf3141", "question_name": "po-scrum_2", "question_path": "PO/po-scrum_2", ' \
                      '"question_type": "CHOICE", "topic": "Product Owner", "disabled": false, "provisioned": 0, ' \
                      '"attempts": 0, "passes": 0, "failures": 0, "question": "The following are roles in the Scrum ' \
                      'Team EXCEPT ___.", "choices": ["Scrum Master", "Development Team", "Project Manager", ' \
                      '"Product Owner"], "answer": 2, "explanation": ["The Project Manager is not part of the scrum ' \
                      'team."], "estimated_time_to_complete": 0.9, "points": 1, "weight": 1.0, "q_weight_scalar": ' \
                      '1.0}, ' \
                      '{"_id": "5fa0d2848bc9ca2dd424816b", "old_id": "BPO_0066", "rel-link_id": ' \
                      '"5ee9250b761cf87371cf3153", "question_name": "po-tech-debt_3", "question_path": ' \
                      '"PO/po-tech-debt_3", "question_type": "CHOICE", "topic": "Product Owner", "disabled": false, ' \
                      '"provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "Which of the ' \
                      'following is a reason for the Product Owner to pay attention to technical debt?", "choices": ' \
                      '["To track the performance of individual developers.", "To understand how velocity might be ' \
                      'affected in the future because of having too much technical debt.", "There\'s no reason for ' \
                      'the Product Owner to pay attention to technical aspects.", "None of the above"], "answer": 1, ' \
                      '"explanation": ["Technical debt is any shortcoming in the solution that makes it less than ' \
                      '\'good enough\' and might create problems in the future. When there\'s a lot of technical ' \
                      'debt, the velocity might decrease in the future because developers have to deal with the ' \
                      'shortcomings. On the other hand, this debt might even remain in the final product and make it ' \
                      'harder and more expensive to maintain the product. Refactoring is a common way of reducing ' \
                      'technical debt. It\'s when the code is improved without changing its external behavior. It\'s ' \
                      'a good idea to Agile teams to spend some of their capacity on continuous refactoring. ' \
                      'Improving the definition of \'Done\' is the second way of reducing technical debt."], ' \
                      '"estimated_time_to_complete": 0.9, "points": 1, "weight": 1.0, "q_weight_scalar": 1.0}, ' \
                      '{"_id": "5fa0d2838bc9ca2dd4248009", "old_id": "BPO_0034", "rel-link_id": ' \
                      '"5ee9250b761cf87371cf3128", "question_name": "po-responsibilities_2", "question_path": ' \
                      '"PO/po-responsibilities_2", "question_type": "CHOICE", "topic": "Product Owner", "disabled": ' \
                      'false, "provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "The Product ' \
                      'Owner makes the ultimate decision on the definition of \'Done\' due to being accountable for ' \
                      'the end result.", "choices": ["True", "False"], "answer": 1, "explanation": ["There may be ' \
                      'multiple definitions of \'Done\' for each development team or a single definition for one ' \
                      'team accross multiple projects. In either case, the development team has the shared ' \
                      'responsibility to create a definition of done."], "estimated_time_to_complete": 0.9, ' \
                      '"points": 1, "weight": 1.0, "q_weight_scalar": 1.0}, ' \
                      '{"_id": "5fa0d2848bc9ca2dd42480c3", "old_id": "BPO_0043", "rel-link_id": ' \
                      '"5ee9250b761cf87371cf313d", "question_name": "po-responsibilities_11", "question_path": ' \
                      '"PO/po-responsibilities_11", "question_type": "CHOICE", "topic": "Product Owner", "disabled": ' \
                      'false, "provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "When does ' \
                      'the Development Team prepare the tools and infrastructure for the development?", "choices": ' \
                      '["Throughout the project", "Before the first Sprint", "In Sprint Zero", "None of the above"], ' \
                      '"answer": 0, "explanation": ["We do not prepare the infrastructure and tools upfront because ' \
                      'it requires an understanding of the whole product, which is against the adaptive concept. ' \
                      'The required infrastructure and tools depend on the product we\'re going to create, which in ' \
                      'turn is defined through the project. The infrastructure and tools will be prepared gradually ' \
                      'though the project. There\'s no Sprint zero in Scrum. All Sprints are the same; to create an ' \
                      'Increment of working software."], "estimated_time_to_complete": 0.9, "points": 1, "weight": ' \
                      '1.0, "q_weight_scalar": 1.0}], ' \
                      '[{"_id": "5fa0d2848bc9ca2dd42480b1", "old_id": "BPO_0040", "rel-link_id": ' \
                      '"5ee9250b761cf87371cf313a", "question_name": "po-responsibilities_8", "question_path": ' \
                      '"PO/po-responsibilities_8", "question_type": "CHOICE", "topic": "Product Owner", "disabled": ' \
                      'false, "provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "All ' \
                      'interactions between the stakeholders and the Development Team should be through the Product ' \
                      'Owner.", "choices": ["True", "False"], "answer": 1, "explanation": ["The interactions can be ' \
                      'through the Product Owner or during the Sprint Review. Other interactions are discouraged as ' \
                      'they distract the developers."], "estimated_time_to_complete": 0.9, "points": 1, "weight": ' \
                      '1.0, "q_weight_scalar": 1.0}, ' \
                      '{"_id": "5fa0d2838bc9ca2dd4247fa9", "old_id": "BPO_0037", "rel-link_id": ' \
                      '"5ee9250a761cf87371cf311a", "question_name": "po-responsibilities_5", "question_path": ' \
                      '"PO/po-responsibilities_5", "question_type": "CHOICE", "topic": "Product Owner", "disabled": ' \
                      'false, "provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "Which ' \
                      'statement is LEAST accurate about the definition of \'Done\'?", "choices": ["It may evolve ' \
                      'during the project", "It\'s created by the Product Owner", "It may vary depending on the ' \
                      'project", "It may be defined by the development organization"], "answer": 1, "explanation": ' \
                      '["The definition of \'Done\' can be prepared in the development organization and used in all ' \
                      'projects as a minimum. If additional content is required for the definition or it\'s not ' \
                      'prepared in the organization level, the Development Team would be responsible for preparing ' \
                      'it. The definition of \'Done\' can evolve during the project as more is learned. It\'s also ' \
                      'dependent on the project and its environment."], "estimated_time_to_complete": 0.9, "points": ' \
                      '1, "weight": 1.0, "q_weight_scalar": 1.0}, ' \
                      '{"_id": "5fa0d2848bc9ca2dd42480db", "old_id": "BPO_0044", "rel-link_id": ' \
                      '"5ee9250b761cf87371cf313f", "question_name": "po-responsibilities_13", ' \
                      '"question_path": "PO/po-responsibilities_13", "question_type": "CHOICE", "topic": ' \
                      '"Product Owner", "disabled": false, "provisioned": 0, "attempts": 0, "passes": 0, "failures": ' \
                      '0, "question": "Which of the following is a responsibility of the Project Manager in Scrum?", ' \
                      '"choices": ["There\'s no Project Manager in Scrum", "Making high-level decisions", ' \
                      '"Authorizing Sprints", "Planning releases", "Preparing the Project Plan and Gantt Charts"], ' \
                      '"answer": 0, "explanation": ["There are only three roles in Scrum and it\'s not allowed to ' \
                      'add other roles or titles: Product Owner, Scrum Master and Development Team"], ' \
                      '"estimated_time_to_complete": 0.9, "points": 1, "weight": 1.0, "q_weight_scalar": 1.0}, ' \
                      '{"_id": "5fa0d2838bc9ca2dd4247f91", "old_id": "BPO_0015", "rel-link_id": ' \
                      '"5ee9250a761cf87371cf3116", "question_name": "po-product-backlog_2", "question_path": ' \
                      '"PO/po-product-backlog_2", "question_type": "CHOICE", "topic": "Product Owner", "disabled": ' \
                      'false, "provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "The Product ' \
                      'Owner should always compose the Product Backlog items with user stories.", "choices": ' \
                      '["True", "False"], "answer": 1, "explanation": ["Usage of user stories to describe backlog ' \
                      'items is a good idea but not required."], "estimated_time_to_complete": 0.9, "points": 1, ' \
                      '"weight": 1.0, "q_weight_scalar": 1.0}, ' \
                      '{"_id": "5fa0d2848bc9ca2dd4248165", "old_id": "BPO_0048", "rel-link_id": ' \
                      '"5ee9250b761cf87371cf3152", "question_name": "po-responsibilities_17", "question_path": ' \
                      '"PO/po-responsibilities_17", "question_type": "CHOICE", "topic": "Product Owner", "disabled": ' \
                      'false, "provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "Who is ' \
                      'responsible for maximizing the value of the work the Development Team performs?", "choices": ' \
                      '["The Product Owner", "The Scrum Master", "The Development Team", "The Scrum Team"], ' \
                      '"answer": 0, "explanation": ["While you might be tempted to select the Development Team ' \
                      'because they are supposed to be self-organized, this is the responsibility of the Product ' \
                      'Owner. This is so, because the value of the developers\' work is highly dependent on what ' \
                      'sequence of items they are developing and this is decided on by the Product Owner. Not that ' \
                      'the Product Owner does not give the developers a list of items to develop during the Sprint. ' \
                      'The Product Owner only orders the Product Backlog and the developers pick as many items as ' \
                      'they like (from the top of the backlog) in the Sprint Planning meeting."],  ' \
                      '"estimated_time_to_complete":0.9, "points": 1, "weight": 1.0, "q_weight_scalar": 1.0}], ' \
                      '[{"_id": "5fa0d2858bc9ca2dd4248183", "old_id": "BPO_0047", "rel-link_id": ' \
                      '"5ee9250b761cf87371cf3157", "question_name": "po-responsibilities_16", "question_path": ' \
                      '"PO/po-responsibilities_16", "question_type": "CHOICE", "topic": "Product Owner", "disabled": ' \
                      'false, "provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "Which of the ' \
                      'following is the best response from the Product Owner when the developers have difficulty ' \
                      'understanding the expectations in the middle of the Sprint?", "choices": ["Attend their Daily ' \
                      'Scrums and answer their questions", "Ask the Scrum Master to facilitate the interactions ' \
                      'between the Product Owner and the Development Team", "Spend enough time with the team to ' \
                      'explain everything.", "Attend their Daily Scrums and remove their impediments"], "answer": 2, ' \
                      '"explanation": ["The Product Owner is responsible for spending enough time with the ' \
                      'developers to make sure the items are clear and understood. The Daily Scrums are only for the ' \
                      'developers. Others may attend but they won\'t participate."], ' \
                      '"estimated_time_to_complete": 0.9, "points": 1, "weight": 1.0, "q_weight_scalar": 1.0}, ' \
                      '{"_id": "5fa0d2848bc9ca2dd42480e7", "old_id": "BPO_0060", "rel-link_id": ' \
                      '"5ee9250b761cf87371cf3140", "question_name": "po-sprint-retrospective_1", "question_path": ' \
                      '"PO/po-sprint-retrospective_1", "question_type": "CHOICE", "topic": "Product Owner", ' \
                      '"disabled": false, "provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": ' \
                      '"What should the Product Owner do during the Sprint Retrospective?", "choices": ["Capture the ' \
                      'Development Team\'s answers", "Participate as a Scrum Team member", "Prioritize the outputs ' \
                      'of the meeting", "Summarize and report the results to the senior management"], "answer": 1, ' \
                      '"explanation": ["The whole Scrum Team should participate in the Sprint Retrospective to find ' \
                      'an improvement to be applied in the next Sprint; including the Product Owner."], ' \
                      '"estimated_time_to_complete": 0.9, "points": 1, "weight": 1.0, "q_weight_scalar": 1.0}, ' \
                      '{"_id": "5fa0d2838bc9ca2dd4247fdf", "old_id": "BPO_0041", "rel-link_id": ' \
                      '"5ee9250a761cf87371cf3122", "question_name": "po-responsibilities_9", "question_path": ' \
                      '"PO/po-responsibilities_9", "question_type": "CHOICE", "topic": "Product Owner", "disabled": ' \
                      'false, "provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "Which ' \
                      'activity is NOT the responsibility of the Product Owner?", "choices": ["Make technical ' \
                      'decisions", "Prioritize the Product Backlog", "Break down Epics into User Stories", "Create ' \
                      'User Stories"], "answer": 0, "explanation": ["Designing the software is part of the ' \
                      'development and should be done by the developers. They are self-organized and make technical ' \
                      'decisions and volunteer for tasks (assign them to themselves). Estimation is the ' \
                      'responsibility of the developers because they are the people who will develop the item. The ' \
                      'rest are done by the Product Owner."], "estimated_time_to_complete": 0.9, "points": 1, ' \
                      '"weight": 1.0, "q_weight_scalar": 1.0}, ' \
                      '{"_id": "5fa0d2838bc9ca2dd4247ffd", "old_id": "BPO_0065", "rel-link_id": ' \
                      '"5ee9250b761cf87371cf3126", "question_name": "po-tech-debt_2", "question_path": ' \
                      '"PO/po-tech-debt_2", "question_type": "CHOICE", "topic": "Product Owner", "disabled": false, ' \
                      '"provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "Why would technical ' \
                      'debt be important for Product Owners?", "choices": ["Because it increases the \'total cost of ' \
                      'ownership\'", "It wouldn\'t; the Product Owner should be focused on value", "Because it ' \
                      'increases value", "All of the above", "None of the above"], "answer": 0, "explanation": ' \
                      '["Technical debt is shortcomings in the code that makes it less than \'good enough\' and ' \
                      'therefore creates risks in the future. It usually decreases velocity in the future (because ' \
                      'the team has to fix the shortcomings) and as some of the debt might remain in the file ' \
                      'product, the maintenance cost will increase as well. Total cost of ownership is a combination ' \
                      'of the project cost and maintenance cost."], "estimated_time_to_complete": 0.9, "points": 1, ' \
                      '"weight": 1.0, "q_weight_scalar": 1.0}, ' \
                      '{"_id": "5fa0d2838bc9ca2dd4247fc1", "old_id": "BPO_0025", "rel-link_id": ' \
                      '"5ee9250a761cf87371cf311e", "question_name": "po-product-backlog_12", "question_path": ' \
                      '"PO/po-product-backlog_12", "question_type": "CHOICE", "topic": "Product Owner", "disabled": ' \
                      'false, "provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "The Product ' \
                      'Owner \'owns\' the Product Backlog.", "choices": ["True", "False"], "answer": 0, ' \
                      '"explanation": ["The Product Owner is the sole responsible person and they even \'own\' the ' \
                      'Product Backlog. Note: remember that the Sprint Backlog is owned by the Development Team. ' \
                      'This is not in contrast to the fact that the Product Backlog items are estimated by the ' \
                      'developers; they provide this information to the Product Owner to be included in the Product ' \
                      'Backlog. There are multiple implications by saying that the Product Owner \'owns\' the ' \
                      'Product Backlog or is the sole responsible person, including: (a) Only they can add items to ' \
                      'the backlog or decompose them into smaller items; while this is done by input from the ' \
                      'customers and developers, and even the Product Owner can delegate this responsibility to the ' \
                      'developers. (b) Only they can decide on the order of the items on the Product Backlog, while ' \
                      'this is usually based on the input from customers and developers."], ' \
                      '"estimated_time_to_complete": 0.9, "points": 1, "weight": 1.0, "q_weight_scalar": 1.0}], ' \
                      '[{"_id": "5fa0d2858bc9ca2dd42481a1", "old_id": "BPO_0063", "rel-link_id": ' \
                      '"5ee9250b761cf87371cf315b", "question_name": "po-stakeholder_2", "question_path": ' \
                      '"PO/po-stakeholder_2", "question_type": "CHOICE", "topic": "Product Owner", "disabled": ' \
                      'false, "provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "Which of the ' \
                      'following are ways for the stakeholders to interact with the Development Team?", "choices": ' \
                      '["At the Sprint Retrospective", "Via the Product Owner", "At the Sprint Review", "Both A and ' \
                      'C", "Both B and C"], "answer": 4, "explanation": ["We don\'t want the team to be distracted; ' \
                      'they need to stay focused and productive. That\'s why their direct interactions with the ' \
                      'customer is limited to the Sprint Review or via the Product Owner."], ' \
                      '"estimated_time_to_complete": 0.9, "points": 1, "weight": 1.0, "q_weight_scalar": 1.0}, ' \
                      '{"_id": "5fa0d2858bc9ca2dd4248189", "old_id": "BPO_0012", "rel-link_id": ' \
                      '"5ee9250b761cf87371cf3158", "question_name": "po-po-role_1", "question_path": ' \
                      '"PO/po-po-role_1", "question_type": "CHOICE", "topic": "Product Owner", "disabled": false, ' \
                      '"provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "Who represents the ' \
                      'interests of the stakeholders?", "choices": ["Scrum Team", "Development Team", "Scrum ' \
                      'Master", "Product Owner"], "answer": 3, "explanation": ["The Product Owner should be in ' \
                      'constant contact with the stakeholders and update the backlog to reflect that interaction."], ' \
                      '"estimated_time_to_complete": 0.9, "points": 1, "weight": 1.0, "q_weight_scalar": 1.0}, ' \
                      '{"_id": "5fa0d2838bc9ca2dd4247f9d", "old_id": "BPO_0059", "rel-link_id": ' \
                      '"5ee9250a761cf87371cf3118", "question_name": "po-sprint-planning_1", "question_path": ' \
                      '"PO/po-sprint-planning_1", "question_type": "CHOICE", "topic": "Product Owner", "disabled": ' \
                      'false, "provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "Which of the ' \
                      'following must be done before starting Sprint Planning?", "choices": ["Assigning Product ' \
                      'Backlog Items to developers", "Arranging enough items at the top to fit in one Sprint", ' \
                      '"Ensuring all requirements are reflected in the Product Backlog", "All of the above", "None ' \
                      'of the above"], "answer": 4, "explanation": ["Although it\'s desireable to have ready items ' \
                      'at the top of the Product Backlog, nothing stops the flow of sprints. If items are not ready ' \
                      'then they are added to the sprint for the purposes of refining them so they can be completed ' \
                      'later. \'Ready\' items are those that are clear and small enough to fit into one Sprint."], ' \
                      '"estimated_time_to_complete": 0.9, "points": 1, "weight": 1.0, "q_weight_scalar": 1.0}, ' \
                      '{"_id": "5fa0d2848bc9ca2dd424809f", "old_id": "BPO_0020", "rel-link_id": ' \
                      '"5ee9250b761cf87371cf3137", "question_name": "po-product-backlog_7", "question_path": ' \
                      '"PO/po-product-backlog_7", "question_type": "CHOICE", "topic": "Product Owner", "disabled": ' \
                      'false, "provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "It is OK for ' \
                      'the items at the bottom of the Product Backlog not to be as clear as those at the top.", ' \
                      '"choices": ["True", "False"], "answer": 0, "explanation": ["Items at the bottom of the ' \
                      'Product Backlog won\'t be developed soon, so, there\'s no need to spend too much time making ' \
                      'them clear and add details; that would be some kind of upfront planning. Not that the items ' \
                      'do not go to the bottom of the backlog because they are not clear or do not have enough ' \
                      'detail; the only basis for order of items is the business value. When the item does not have ' \
                      'enough detail and starts to go to the top of the Product Backlog, it should be refined by ' \
                      'adding more detail."], "estimated_time_to_complete": 0.9, "points": 1, "weight": 1.0, ' \
                      '"q_weight_scalar": 1.0}, ' \
                      '{"_id": "5fa0d2838bc9ca2dd424803f", "old_id": "BPO_0029", "rel-link_id": ' \
                      '"5ee9250b761cf87371cf312d", "question_name": "po-product-backlog_16", "question_path": ' \
                      '"PO/po-product-backlog_16", "question_type": "CHOICE", "topic": "Product Owner", "disabled": ' \
                      'false, "provisioned": 0, "attempts": 0, "passes": 0, "failures": 0, "question": "How should a ' \
                      'Product Backlog Item be refined when it is located at the top?", "choices": ["All the tasks ' \
                      'required for completing it should be identified", "In a way that it can be \'Done\' in the ' \
                      'time-boxed duration of one Sprint", "In a way that it\'s clear enough for the developers", ' \
                      '"Both B and C", "Both A and B"], "answer": 3, "explanation": ["Items at the top of the ' \
                      'Product Backlog should be small enough to fit in one Sprint. They should also be clear and ' \
                      'detailed enough; but not too detailed (e.g. not with all tasks need to be identified) as it ' \
                      'would be upfront planning. Tasks are created gradually during the Sprint. Product Backlog ' \
                      'items require multiple expertise and usually done by multiple developers. Therefore, they are ' \
                      'not assigned. Tasks, however, are assigned to the individual developers, while everyone stays ' \
                      'accountable."], "estimated_time_to_complete": 0.9, "points": 1, "weight": 1.0, ' \
                      '"q_weight_scalar": 1.0}]]'

    @classmethod
    def setUpClass(cls) -> None:
        create_files(os.path.join(cls.testing_dir, cls.exams_file_one), cls.gend_exams_one)
        create_files(os.path.join(cls.testing_dir, cls.exams_file_two), cls.gend_exams_two)
        create_files(os.path.join(cls.testing_dir, cls.exams_file_five), cls.gend_exams_five)

    @classmethod
    def tearDownClass(cls) -> None:
        remove_temp_files(os.path.join(cls.testing_dir))

    def test_get_exam_list_one_exam(self):
        with open(os.path.join(self.testing_dir, self.exams_file_one), "r") as exams_fp:
            self.expected_list = json.load(exams_fp)
        self.assertListEqual(self.expected_list,
                             generate_gsuite_exam.get_exam_list(os.path.join(self.testing_dir, self.exams_file_one)),
                             msg="Mismatch in resulting exam list")

    def test_get_exam_list_two_exams(self):
        with open(os.path.join(self.testing_dir, self.exams_file_two), "r") as exams_fp:
            self.expected_list = json.load(exams_fp)
        self.assertListEqual(self.expected_list,
                             generate_gsuite_exam.get_exam_list(os.path.join(self.testing_dir, self.exams_file_two)),
                             msg="Mismatch in resulting exam list")

    def test_get_exam_list_five_exams(self):
        with open(os.path.join(self.testing_dir, self.exams_file_five), "r") as exams_fp:
            self.expected_list = json.load(exams_fp)
        self.assertListEqual(self.expected_list,
                             generate_gsuite_exam.get_exam_list(os.path.join(self.testing_dir, self.exams_file_five)),
                             msg="Mismatch in resulting exam list")

    def test_get_exam_list_empty_string(self):
        self.expected_list = []
        self.assertListEqual(self.expected_list,
                             generate_gsuite_exam.get_exam_list(""),
                             msg="Mismatch in resulting exam list")

    def test_get_exam_list_no_exist(self):
        self.assertRaises(FileNotFoundError, generate_gsuite_exam.get_exam_list, "this_doesnt_exist.json")
