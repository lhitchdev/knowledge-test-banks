import unittest
import unittest.mock as mock
import sys
import os
import io
import json
import shutil
import copy
import datetime
import pymongo
# Move up the necessary amount of directories so the scripts path is inserted into sys.path
# In this case, it takes 4 directories to get to the scripts folder
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))))
from formal_exam_pres.exam_content_builder import build_exam_content, eval_builder

# # Get Mongo DB connection
# host = os.getenv('MONGO_HOST', "localhost")
# port = int(os.getenv('MONGO_PORT', "27017"))
# client = pymongo.MongoClient(host, port)
# db_testbank = client["knowledge_test_bank"]

exam_stats = {
    "exams_generated": 0,
    "exams_to_gen": 1,
    "time_allowed": 90,
    "delta": 0.2,
    "topics": {
        "Algorithms": 3,
        "Assembly": 5,
        "C Programming": 15,
        "Data Structures": 3,
        "Git": 3,
        "Networking": 5,
        "Python": 15,
        "Software Engineering Fundamentals": 1
    }
}


questions = [
    {
        "_id": "5fa0d2858bc9ca2dd42481f5",
        "old_id": "BD_ASM_0002",
        # "rel-link_id": "5ee9250b761cf87371cf3168",
        "question_name": "assembly-arithmetic-instructions_1",
        "question_path": "CCD/Assembly/assembly-arithmetic-instructions_1",
        "question_type": "knowledge",
        "topic": "Assembly",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": "In the above Assembly code, what does eax contain after all the operations?",
        "snippet": "1 | mov eax, 12\n"
                   "2 | mov ebx, 4\n"
                   "3 | add eax, ebx\n"
                   "4 | inc ebx\n"
                   "5 | inc eax\n"
                   "6 | shr eax, 1\n"
                   "7 | ",
        "snippet_lang": "nasm",
        "choices": [
            "17",
            "16",
            "8",
            "18"
        ],
        "answer": 2,
        "explanation": [
            "Line 1 moves the value 12 into the eax register and line 2 moves the value 4 into the ebx register. Line "
            "3 adds the two values, storing the result (16) in the eax register. Line 4 can be ignored since the ebx "
            "register isn't used anymore. Line 5 increments the eax register value by one, now (17), and line 6 moves "
            "bit values right one spot, from 0001 0001 (17) to 0000 1000 (8)."
        ]
    },
    {
        "_id": "5fa0d2858bc9ca2dd424826d",
        "old_id": "BD_ASM_0007",
        # "rel-link_id": "5ee9250b761cf87371cf317c",
        "question_name": "assembly-arithmetic-instructions_6",
        "question_path": "CCD/Assembly/assembly-arithmetic-instructions_6",
        "question_type": "knowledge",
        "topic": "Assembly",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": "The primary opcode to increment a number in a register by 1 is:",
        "choices": [
            "++",
            "inc",
            "dec",
            "mov"
        ],
        "answer": 1,
        "explanation": [
            "Incorrect: ++ is not a valid assembly opcode",
            "Correct: inc is the assembly opcode to increment a number by 1",
            "Incorrect: dec is the assembly opcode to decrement a number by 1",
            "Incorrect: mov puts a value (replaces current value) in a register"
        ]
    },
    {
        "_id": "5fa0d2868bc9ca2dd424829d",
        "old_id": "BD_ASM_0017",
        # "rel-link_id": "5ee9250b761cf87371cf3184",
        "question_name": "assembly-computer-basics_1",
        "question_path": "CCD/Assembly/assembly-computer-basics_1",
        "question_type": "knowledge",
        "topic": "Assembly",
        "disabled": False,
        "provisioned": 1,
        "attempts": 2,
        "passes": 2,
        "failures": 0,
        "question": "The above Assembly code stores a memory address in the rax register.  Which instruction would "
                    "store data at that address?",
        "snippet": "1 | mov rax, 0xc0ffee\n"
                   "2 | ",
        "snippet_lang": "nasm",
        "choices": [
            "mov rax, 100",
            "mov [rax], 100",
            "add rax, 100",
            "mov 0xc0ffee, 100"
        ],
        "answer": 1,
        "explanation": [
            "Incorrect: this instruction would replace the memory address in rax with the value 100",
            "Correct: to access the data for a given memory address, you use the [] brackets",
            "Incorrect: this command would simply modify the memory address",
            "Incorrect: the mov command needs to be followed by a valid operand (in this case a valid register name)"
        ]
    },
    {
        "_id": "5fa0d2888bc9ca2dd4248573",
        "old_id": "BD_C_0023",
        # "rel-link_id": "5ee9250b761cf87371cf31f2",
        "question_name": "c-directives_1",
        "question_path": "CCD/C_Programming/c-group_1/c-directives_1",
        "question_type": "knowledge",
        "topic": "C Programming",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": "Which line of code contains a preprocessor directive that creates a named constant?",
        "snippet": "1  | #include <stdio.h>\n"
                   "2  | #include <stdlib.h>\n"
                   "3  | \n"
                   "4  | #define y 10\n"
                   "5  | \n"
                   "6  | float getQuotient(int, int);\n"
                   "7  | \n"
                   "8  | int main()\n"
                   "9  | {\n"
                   "10 |     int x = rand();\n"
                   "11 | \n"
                   "12 |     float z = getQuotient(x,y);\n"
                   "13 |     \n"
                   "14 |     float* p = (float *) malloc(y * sizeof(float));\n"
                   "15 |     \n"
                   "16 |     p[0] = z;\n"
                   "17 |     \n"
                   "18 |     printf(\"Result is %f\", *p);\n"
                   "19 |     \n"
                   "20 |     return 0;\n"
                   "21 | }\n"
                   "22 | \n"
                   "23 | float getQuotient(int numerator, int denominator)\n"
                   "24 | {\n"
                   "25 |     return (float)numerator/denominator;\n"
                   "26 | }\n"
                   "27 | ",
        "snippet_lang": "c",
        "choices": [
            "2",
            "10",
            "4",
            "16"
        ],
        "answer": 2,
        "explanation": [
            "Incorrect: although line 2 is using a preprocessor directive, it is an include that is used to use other "
            "header files in your program",
            "Incorrect",
            "Correct: #define is a preprocessor directive that allows you to create a named constant",
            "Incorrect"
        ],
        "group": {
            "group_name": "c-group_1",
            "questions": [
                "BD_C_0024",
                "BD_C_0027",
                "BD_C_0022",
                "BD_C_0035",
                "BD_C_0029",
                "BD_C_0031",
                "BD_C_0026",
                "BD_C_0023",
                "BD_C_0025",
                "BD_C_0034",
                "BD_C_0032",
                "BD_C_0030",
                "BD_C_0033",
                "BD_C_0028"
            ]
        }
    },
    {
        "_id": "5fa0d2888bc9ca2dd424856d",
        "old_id": "BD_C_0032",
        # "rel-link_id": "5ee9250b761cf87371cf31f1",
        "question_name": "c-pointers_1",
        "question_path": "CCD/C_Programming/c-group_1/c-pointers_1",
        "question_type": "knowledge",
        "topic": "C Programming",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": "Which of the following variables contain a memory address?",
        "snippet": "1  | #include <stdio.h>\n"
                   "2  | #include <stdlib.h>\n"
                   "3  | \n"
                   "4  | #define y 10\n"
                   "5  | \n"
                   "6  | float getQuotient(int, int);\n"
                   "7  | \n"
                   "8  | int main()\n"
                   "9  | {\n"
                   "10 |     int x = rand();\n"
                   "11 | \n"
                   "12 |     float z = getQuotient(x,y);\n"
                   "13 |     \n"
                   "14 |     float* p = (float *) malloc(y * sizeof(float));\n"
                   "15 |     \n"
                   "16 |     p[0] = z;\n"
                   "17 |     \n"
                   "18 |     printf(\"Result is %f\", *p);\n"
                   "19 |     \n"
                   "20 |     return 0;\n"
                   "21 | }\n"
                   "22 | \n"
                   "23 | float getQuotient(int numerator, int denominator)\n"
                   "24 | {\n"
                   "25 |     return (float)numerator/denominator;\n"
                   "26 | }\n"
                   "27 | ",
        "snippet_lang": "c",
        "choices": [
            "p",
            "z",
            "y",
            "All the above"
        ],
        "answer": 0,
        "explanation": [
            "Correct: p is a pointer, and pointers can contain a memory address",
            "Incorrect: z is not a pointer and therefore cannot contain a memory address",
            "Incorrect: y is not a pointer and therefore cannot contain a memory address"
        ],
        "group": {
            "group_name": "c-group_1",
            "questions": [
                "BD_C_0024",
                "BD_C_0027",
                "BD_C_0022",
                "BD_C_0035",
                "BD_C_0029",
                "BD_C_0031",
                "BD_C_0026",
                "BD_C_0023",
                "BD_C_0025",
                "BD_C_0034",
                "BD_C_0032",
                "BD_C_0030",
                "BD_C_0033",
                "BD_C_0028"
            ]
        }
    },
    {
        "_id": "5fa0d2888bc9ca2dd42485bb",
        "old_id": "BD_C_0035",
        # "rel-link_id": "5ee9250b761cf87371cf31fe",
        "question_name": "c-stack_1",
        "question_path": "CCD/C_Programming/c-group_1/c-stack_1",
        "question_type": "knowledge",
        "topic": "C Programming",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": "Which of the following variables allocate additional non-stack based memory?",
        "snippet": "1  | #include <stdio.h>\n"
                   "2  | #include <stdlib.h>\n"
                   "3  | \n"
                   "4  | #define y 10\n"
                   "5  | \n"
                   "6  | float getQuotient(int, int);\n"
                   "7  | \n"
                   "8  | int main()\n"
                   "9  | {\n"
                   "10 |     int x = rand();\n"
                   "11 | \n"
                   "12 |     float z = getQuotient(x,y);\n"
                   "13 |     \n"
                   "14 |     float* p = (float *) malloc(y * sizeof(float));\n"
                   "15 |     \n"
                   "16 |     p[0] = z;\n"
                   "17 |     \n"
                   "18 |     printf(\"Result is %f\", *p);\n"
                   "19 |     \n"
                   "20 |     return 0;\n"
                   "21 | }\n"
                   "22 | \n"
                   "23 | float getQuotient(int numerator, int denominator)\n"
                   "24 | {\n"
                   "25 |     return (float)numerator/denominator;\n"
                   "26 | }\n"
                   "27 | ",
        "snippet_lang": "c",
        "choices": [
            "z",
            "p",
            "denominator",
            "All the above"
        ],
        "answer": 1,
        "explanation": [
            "All locally declared variables have storage space allocated for them on the stack. However, additional "
            "space may be allocated on the heap through the use of functions like malloc(). In this case, p is a local "
            "variable and the storage for the variable itself is allocated on the stack. Since malloc() is called, it "
            "allocates additional memory on the heap and stores that address into p."
        ],
        "group": {
            "group_name": "c-group_1",
            "questions": [
                "BD_C_0024",
                "BD_C_0027",
                "BD_C_0022",
                "BD_C_0035",
                "BD_C_0029",
                "BD_C_0031",
                "BD_C_0026",
                "BD_C_0023",
                "BD_C_0025",
                "BD_C_0034",
                "BD_C_0032",
                "BD_C_0030",
                "BD_C_0033",
                "BD_C_0028"
            ]
        }
    },
    {
        "_id": "5fa0d2878bc9ca2dd42483f3",
        "old_id": "BD_DSTRUC_0002",
        # "rel-link_id": "5ee9250b761cf87371cf31b7",
        "question_name": "circularly-linked-list",
        "question_path": "CCD/Data_Structures/data-structures-circularly-linked-lists",
        "question_type": "knowledge",
        "topic": "Data Structures",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": "What does the last node of a multi-node circularly linked list point to?",
        "choices": [
            "The first node",
            "NULL",
            "Itself",
            "The previous node"
        ],
        "answer": 0,
        "explanation": [
            "Circularly linked nodes use nodes similar to regular linked lists except the last node in a circularly "
            "linked list points to the first node - no node points to NULL"
        ]
    },
    {
        "_id": "5fa0d2878bc9ca2dd42483e7",
        "old_id": "BD_DSTRUC_0005",
        # "rel-link_id": "5ee9250b761cf87371cf31b5",
        "question_name": "queue",
        "question_path": "CCD/Data_Structures/data-structure-queue",
        "question_type": "knowledge",
        "topic": "Data Structures",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": "Which of the following is a proper behavior for a queue?",
        "choices": [
            "Items can be added to the front of the queue if it's implemented with a linked-list.",
            "Items are removed from the front of the queue.",
            "A queue is a Last In, First Out data structure.",
            "Both B & C are correct."
        ],
        "answer": 1,
        "explanation": [
            "Only the first item in the queue (front) can be removed via a dequeue function for a queue.  A queue is "
            "first in first out data structure."
        ]
    },
    {
        "_id": "5fa0d2878bc9ca2dd42483ff",
        "old_id": "BD_DSTRUC_0007",
        # "rel-link_id": "5ee9250b761cf87371cf31b9",
        "question_name": "tree",
        "question_path": "CCD/Data_Structures/data-structures-tree",
        "question_type": "knowledge",
        "topic": "Data Structures",
        "disabled": False,
        "provisioned": 0,
        "attempts": 0,
        "passes": 0,
        "failures": 0,
        "question": "The above class is ideal for what type of data structure?",
        "snippet": "1 | class Node:\n "
                   "2 |     def __init__(self, data):\n "
                   "3 |         self.data = data\n "
                   "4 |         self.children = []\n",
        "snippet_lang": "C",
        "choices": [
            "linked-list.",
            "doubly-linked list.",
            "general tree",
            "binary tree"
        ],
        "answer": 2,
        "explanation": [
            "Incorrect: A linked list has a node typically consisting of data and a next ref to the next node or to "
            "None.",
            "Incorrect: A doubly-linked list has a node typically consisting of data and a next reference the next "
            "node or to None and a prev reference to the previous node or None.",
            "Correct: A general tree is a data structure where every child has only one parent, but one parent can "
            "have many children.",
            "Incorrect: although this could be used for a binary tree, binary tree are limited parents have two "
            "children, therefore the node would consist of a left reference the left node/child or to None and a right "
            "reference to the right node/child or None."
        ]
    }
]


def create_files(path: str, data):
    os.mkdir(os.path.dirname(path))
    with open(path, "w") as path_fp:
        json.dump(data, path_fp)


def remove_temp_files(temp_folder: str):
    if os.path.exists(temp_folder):
        shutil.rmtree(temp_folder)


class GetTestGenStatsTests(unittest.TestCase):
    testing_dir = "Testing_get_test_gen_stats_Files"
    stats_file = "test_exam_gen_stats_ccd.json"
    expected_exam_gen_stats = copy.deepcopy(exam_stats)

    @classmethod
    def setUpClass(cls) -> None:
        create_files(os.path.join(cls.testing_dir, cls.stats_file), cls.expected_exam_gen_stats)

    @classmethod
    def tearDownClass(cls) -> None:
        remove_temp_files(cls.testing_dir)

    def test_get_test_gen_stats_rtn(self):
        print(os.getcwd())
        self.assertDictEqual(self.expected_exam_gen_stats,
                             build_exam_content.get_test_gen_stats(os.path.join(self.testing_dir, self.stats_file)),
                             "exam_gen_stats file mismatch")

    def test_get_test_gen_stats_raise(self):
        error_msg = "Unable to load Invalid_File: .*$"
        with self.assertRaises(ValueError) as cm:
            build_exam_content.get_test_gen_stats("Invalid_File")
        self.assertRegex(cm.exception.args[0], error_msg, "Error message mismatch in get_test_gen_stats")


class GetCurTopicsTests(unittest.TestCase):
    def setUp(self) -> None:
        self.topics = ["Python", "Git", "Data Structures"]
        self.expected_topic_dict = {
            "topics": {
                "Python": 0,
                "Git": 0,
                "Data Structures": 0
            }
        }

    def test_get_cur_topics(self):
        self.assertDictEqual(self.expected_topic_dict, build_exam_content.get_cur_topics(self.topics))


class UpdateTopicsTests(unittest.TestCase):
    def setUp(self) -> None:
        self.src_topics = {
            "topics": {
                "Python": 0,
                "Git": 0,
                "Data Structures": 0,
                "Something New": 0,
                "Another New Thing": 0
            }
        }
        self.dst_stats = copy.deepcopy(exam_stats)
        self.expected_topics = {
            "exams_generated": 0,
            "exams_to_gen": 1,
            "time_allowed": 90,
            "delta": 0.2,
            "topics": {
                "Python": 15,
                "Git": 3,
                "Data Structures": 3,
                "Something New": 0,
                "Another New Thing": 0,
                "Algorithms": 3,
                "Assembly": 5,
                "C Programming": 15,
                "Networking": 5,
                "Software Engineering Fundamentals": 1
            }
        }

    def test_update_topics(self):
        build_exam_content.update_topics(self.src_topics, self.dst_stats)
        self.assertDictEqual(self.expected_topics, self.dst_stats)


class CheckOldTopicsTests(unittest.TestCase):
    def setUp(self) -> None:
        self.current_topics = {
            "topics": {
                "Python": 0,
                "Git": 0,
                "Data Structures": 0
            }
        }
        self.exam_stats = copy.deepcopy(exam_stats)
        self.exam_stats["topics"]["Networking"] = 0
        self.exam_stats["topics"]["Software Engineering Fundamentals"] = 0

    def test_check_old_topics(self):
        expected_msgs = [
            f"WARNING: The exam generation stats file is requesting 3 'Algorithms' questions; however, there are "
            "currently ZERO questions in the test bank using this topic.",
            f"WARNING: The exam generation stats file is requesting 5 'Assembly' questions; however, there are "
            "currently ZERO questions in the test bank using this topic.",
            f"WARNING: The exam generation stats file is requesting 15 'C Programming' questions; however, there are "
            "currently ZERO questions in the test bank using this topic."
        ]
        with mock.patch('sys.stderr', new=io.StringIO()) as fake_out:
            build_exam_content.check_old_topics(self.current_topics, self.exam_stats)
            self.assertEqual("\n".join(expected_msgs), fake_out.getvalue().strip(), "Mismatch in output message")

    def test_check_old_topics_no_curr_topics(self):
        expected_msgs = [
            f"WARNING: The exam generation stats file is requesting 3 'Algorithms' questions; however, there are "
            "currently ZERO questions in the test bank using this topic.",
            f"WARNING: The exam generation stats file is requesting 5 'Assembly' questions; however, there are "
            "currently ZERO questions in the test bank using this topic.",
            f"WARNING: The exam generation stats file is requesting 15 'C Programming' questions; however, there are "
            "currently ZERO questions in the test bank using this topic.",
            f"WARNING: The exam generation stats file is requesting 3 'Data Structures' questions; however, there are "
            "currently ZERO questions in the test bank using this topic.",
            f"WARNING: The exam generation stats file is requesting 3 'Git' questions; however, there are "
            "currently ZERO questions in the test bank using this topic.",
            f"WARNING: The exam generation stats file is requesting 15 'Python' questions; however, there are "
            "currently ZERO questions in the test bank using this topic."
        ]
        with mock.patch('sys.stderr', new=io.StringIO()) as fake_out:
            build_exam_content.check_old_topics({}, self.exam_stats)
            self.assertEqual("\n".join(expected_msgs), fake_out.getvalue().strip(), "Mismatch in output message")


class CheckUnusedCategoryTests(unittest.TestCase):
    def setUp(self) -> None:
        self.exam_stats = copy.deepcopy(exam_stats)
        self.exam_stats["topics"]["Networking"] = 0
        self.exam_stats["topics"]["Software Engineering Fundamentals"] = 0

    def test_check_unused_category(self):
        expected_msgs = [
            f"WARNING: The exam generation stats file is requesting 0 'Networking' questions.",
            f"WARNING: The exam generation stats file is requesting 0 'Software Engineering Fundamentals' questions."
        ]
        with mock.patch('sys.stderr', new=io.StringIO()) as fake_out:
            build_exam_content.check_unused_category(self.exam_stats)
            self.assertEqual("\n".join(expected_msgs), fake_out.getvalue().strip(), "Mismatch in output message")

    def test_check_unused_category_no_stats(self):
        expected_msgs = ""
        with mock.patch('sys.stderr', new=io.StringIO()) as fake_out:
            build_exam_content.check_unused_category({})
            self.assertEqual("\n".join(expected_msgs), fake_out.getvalue().strip(), "Mismatch in output message")


class SwapDoubleQuotes(unittest.TestCase):
    exam = [
        {
            "_id": "5fa0d2878bc9ca2dd42483f3",
            "old_id": "BD_DSTRUC_0002",
            # "rel-link_id": "5ee9250b761cf87371cf31b7",
            "question_name": "circularly-linked-list",
            "question_path": "CCD/Data_Structures/data-structures-circularly-linked-lists",
            "question_type": "knowledge",
            "topic": "Data Structures",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "What does the \"last\" node of a multi-node circularly linked list point to?",
            "choices": [
                "The \"first\" node",
                "NULL",
                "Itself",
                "The previous node"
            ],
            "answer": 0,
            "explanation": [
                "Circularly linked nodes use nodes similar to regular linked lists except the last node in a "
                "circularly linked list points to the first node - no node points to NULL"
            ]
        }
    ]
    expected_exam = [
        {
            "_id": "5fa0d2878bc9ca2dd42483f3",
            "old_id": "BD_DSTRUC_0002",
            # "rel-link_id": "5ee9250b761cf87371cf31b7",
            "question_name": "circularly-linked-list",
            "question_path": "CCD/Data_Structures/data-structures-circularly-linked-lists",
            "question_type": "knowledge",
            "topic": "Data Structures",
            "disabled": False,
            "provisioned": 0,
            "attempts": 0,
            "passes": 0,
            "failures": 0,
            "question": "What does the \'last\' node of a multi-node circularly linked list point to?",
            "choices": [
                "The \'first\' node",
                "NULL",
                "Itself",
                "The previous node"
            ],
            "answer": 0,
            "explanation": [
                "Circularly linked nodes use nodes similar to regular linked lists except the last node in a "
                "circularly linked list points to the first node - no node points to NULL"
            ]
        }
    ]

    def setUp(self) -> None:
        self.eb = eval_builder.EvalBuilder()
        self.eb.set_question_list(self.exam)
        self.expected_eb = eval_builder.EvalBuilder()
        self.expected_eb.set_question_list(self.expected_exam)

    def test_swap_double_quotes(self):
        test_exam = copy.deepcopy(self.eb.question_list)
        build_exam_content.swap_double_quotes(test_exam)
        # We need to check the EvalQuestion's question_data and a one item list makes this easier
        self.assertDictEqual(self.expected_eb.question_list[0].question_data,
                             test_exam[0].question_data,
                             msg="Mismatch in double quote swapped strings")


class GetExamJsonTests(unittest.TestCase):
    def setUp(self) -> None:
        self.eb = eval_builder.EvalBuilder()
        self.eb.set_question_list(questions)

    def test_get_exam_json_no_mttl(self):
        # Simulates being passed a list of generated exams
        expected_json = [self.eb.get_question_list_dict()]
        expected_json = json.dumps(expected_json)
        # Note, the eb.question_list is put inside another list to simulate the list of exams generated
        self.assertEqual(expected_json, build_exam_content.get_exam_json([self.eb.question_list]))

    def test_get_exam_json_no_mttl_single_list(self):
        # Simulates being passed a list of questions which should return a json string representing an empty list
        expected_json = json.dumps([])
        self.assertEqual(expected_json, build_exam_content.get_exam_json(self.eb.question_list))


class WriteExamJsonTests(unittest.TestCase):
    testing_dir = "Testing_write_exam_json_Files"

    def setUp(self) -> None:
        os.mkdir(self.testing_dir)
        self.eb = eval_builder.EvalBuilder()
        self.eb.set_question_list(questions)
        self.exam_json_string = json.dumps(self.eb.get_question_list_dict())
        self.out_file = os.path.join(self.testing_dir, "test_exam_json_string.json")

    def tearDown(self) -> None:
        remove_temp_files(self.testing_dir)

    def test_write_exam_json(self):
        expected_data = self.exam_json_string
        written_files = []
        build_exam_content.write_exam_json(self.exam_json_string, self.out_file)
        for root, dirs, files in os.walk(self.testing_dir):
            for this_file in files:
                written_files.append(os.path.join(root, this_file))
        self.assertTrue(len(written_files) == 1,
                        msg=f"Mismatch in number of written files; found {len(written_files)} but expected 1")
        self.assertEqual(self.out_file, written_files[0], msg="Mismatch in created files name")
        with open(written_files[0], "r") as fp:
            data = fp.read()
            self.assertEqual(expected_data, data, msg="Mismatch in written data")


class CreateImagesTests(unittest.TestCase):
    def setUp(self) -> None:
        self.testing_dir = "Testing_Create_Image_Files"
        self.exam_gen_stats = copy.deepcopy(exam_stats)
        for topic in self.exam_gen_stats["topics"].keys():
            if topic != "Assembly" and topic != "C Programming" and topic != "Data Structures":
                self.exam_gen_stats["topics"][topic] = 0
            else:
                self.exam_gen_stats["topics"][topic] = 3
        self.eb = eval_builder.EvalBuilder()
        self.eb.set_question_list(questions)
        self.eb.exams_to_generate = self.exam_gen_stats["exams_to_gen"]
        self.eb.time_allowed = self.exam_gen_stats["time_allowed"]
        for topic in sorted(self.exam_gen_stats["topics"]):
            self.eb.register_category(topic, self.exam_gen_stats["topics"][topic])
        self.eb.sort_cat_list_by_value()
        self.exam_q_list = self.eb.create_exam([])
        self.font_file = "../support/font_files/monaco.ttf"

    def tearDown(self) -> None:
        remove_temp_files(self.testing_dir)

    def test_create_image_files(self):
        build_exam_content.create_images(self.exam_q_list, self.testing_dir, font_file=self.font_file)
        q_ids = [question.question_data["_id"] for question in self.exam_q_list if "snippet" in question.question_data]
        expected_image_count = 6
        image_count = 0
        for root, dirs, files in os.walk(self.testing_dir):
            for file_path in files:
                file_parts = os.path.splitext(file_path)
                if file_parts[1].lower() == ".png":
                    image_count += 1
                    self.assertTrue(file_parts[0] in q_ids, msg="Mismatch in generated image name")
        self.assertEqual(expected_image_count, image_count, msg="Mismatch in number of generated images")

    def test_create_image_files_q_data(self):
        build_exam_content.create_images(self.exam_q_list, self.testing_dir, font_file=self.font_file)
        q_ids = [question.question_data["_id"] for question in self.exam_q_list if "snippet" in question.question_data]
        for q_id in q_ids:
            for question in self.exam_q_list:
                if question.question_data["_id"] == q_id:
                    self.assertTrue("image_name" in question.question_data, msg="Missing image name in question data")
        for root, dirs, files in os.walk(self.testing_dir):
            for file_path in files:
                if os.path.splitext(file_path)[1].lower() == ".png":
                    for question in self.exam_q_list:
                        if f"{question.question_data['_id']}.png" == file_path:
                            self.assertEqual(file_path, question.question_data["image_name"],
                                             msg="Mismatch in saved image file name and question data image name")


class GenerateKnowledgeTests(unittest.TestCase):
    def setUp(self) -> None:
        self.testing_dir = "Testing_generate_knowledge_Files"
        os.mkdir(self.testing_dir)
        self.exam_gen_stats = copy.deepcopy(exam_stats)
        for topic in self.exam_gen_stats["topics"].keys():
            if topic != "Assembly" and topic != "C Programming" and topic != "Data Structures":
                self.exam_gen_stats["topics"][topic] = 0
            else:
                self.exam_gen_stats["topics"][topic] = 3
        self.eb = eval_builder.EvalBuilder()
        self.eb.set_question_list(questions)
        self.eb.exams_to_generate = self.exam_gen_stats["exams_to_gen"]
        self.nameof_mqf_file = "DEV_MQF.json"
        self.font_file = "../support/font_files/monaco.ttf"

    def tearDown(self) -> None:
        remove_temp_files(self.testing_dir)

    def test_generate_knowledge(self):
        exam_ver = datetime.datetime.strftime(datetime.datetime.now(), "%Y-%m-%dT%H:%M:")
        file_name = build_exam_content.generate_knowledge(self.exam_gen_stats, self.eb, self.nameof_mqf_file,
                                                          self.testing_dir, font_file=self.font_file)
        self.assertRegex(file_name, r"DEV_MQF_{exam_ver}[0-9][0-9]:[0-9]+.json".format(exam_ver=exam_ver), 
                         msg="Mismatch in generated file name")
        self.assertEqual(self.exam_gen_stats["time_allowed"], self.eb.time_allowed)
        with open(os.path.join(self.testing_dir, file_name), "r") as fp:
            exams = json.load(fp)
        self.assertTrue(type(exams) is list, msg="Written data expected to be a list of exams")
        self.assertEqual(self.eb.exams_generated, len(exams), msg="Mismatch in number of generated exams")
        for exam in exams:
            self.assertTrue(type(exam) is list, msg="Each exam is expected to be a list of questions")
            for question in exam:
                self.assertTrue(type(question) is dict, msg="Exam list expected to contain questions as dict objects")
